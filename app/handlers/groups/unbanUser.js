var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var groupId = new ObjectId(gId);
    var targetId = new ObjectId(tId);


    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var groups = db.collection('groups');
            groups.find({
                $and: [{
                        "_id": groupId
                    }, {
                        $or: [{
                            "moderators": userId
                        }, {
                            "owner": userId
                        }]
                    }

                ]
            }).toArray(function(err, authorityExists) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (authorityExists.length) {
                    //First let us check if user is blocked
                    groups.find({
                        $and: [{
                            "_id": groupId
                        }, {
                            "bannedUsers": targetId
                        }]
                    }).toArray(function(err, isBlocked) {
                        if (err) {
                            res.send(err);
                        } else if (isBlocked.length) {
                            groups.update({
                                "_id": groupId
                            }, {


                                $pull: {
                                    "bannedUsers": targetId,
                                }


                            });
                            res.send("User is unbanned from the group!");

                        } else {
                            res.send("Error! Member is not banned from this group.");

                        }
                    });
                } else {
                    res.send('You are not authorized to unban a member');
                }
            });
        }
    });
};