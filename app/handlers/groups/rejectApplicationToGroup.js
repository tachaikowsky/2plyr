var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var groupId = new ObjectId(gId);
    var invitee = new ObjectId(tId);



    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var groups = db.collection('groups');

            groups.find(
            {
                    $and: [{
                            "_id": groupId
                        }, {
                            $or: [{
                                "moderators": userId
                            }, {
                                "owner": userId
                            }]
                        }

                    ]
                }).toArray(function(err, hasAuthoriy)
            {
                //   console.log(result);
                if (err)
                {
                    res.send(err);
                }
                else if (hasAuthoriy.length)
                {
                    //First let us check if user is a friend
                    groups.find({$and: [
                    {
                        "_id": groupId
                    },
                    {
                        "pendingUsers": invitee
                    }]}).toArray(function(err, isUserPending)
                    {
                        if (err)
                        {
                            res.send(err);
                        }
                        else if (isUserPending.length)
                        {
                             groups.update(
                                    {
                                        "_id": groupId
                                    },
                                    {
                                        $pull:
                                        {
                                            "pendingUsers": invitee,
                                        }
                                    })

                            
                            res.send("Application has been Rejected!")
                                //updating outgoing array to resolve any bugs
                        }
                        else
                        {

                            groups.find({
                                $and: [{
                                    "_id": groupId
                                }, {
                                    $or: [{
                                        members: invitee
                                    }]
                                }]
                            }).toArray(function(err, isMember) {
                                if (err) {
                                    res.send(err);
                                } else if (isMember.length) {
                                    
                                    res.send("Error! Target User is already a member!")
                                        //updating outgoing array to resolve any bugs
                                } else {
                            res.send("Error! No Pending Friend Request found!")

                                }
                            });

                            
                        }
                    });
                }
                else
                {
                    res.send('Error! You have no authority to perform this operation');
                }
            });
        }
    }); 
};