var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function getGroupInfo(groupId, userId, url, callback) {

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        } else {
            console.log('Connection established to', url);

            var groups = db.collection('groups');
            groups.find({
                    "_id": groupId
                }, {
                    "members": 1
                }

            ).toArray(function(err, groupExists) {
                if (err) {
                    res.send(err);
                    return callback(err);
                } else if (groupExists.length) {
                    return (null, groupExists);
                } else {
                    groupExists = 'There is no such group';
                    return (null, groupExists);
                    
                }
            });
        }
    });
}

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var groupId = new ObjectId(gId);

    getGroupInfo(groupId, userId, url, function(err, groupDetails) {
        if (err) {
            res.send(err);
        }

        res.send(groupDetails);
    });

};