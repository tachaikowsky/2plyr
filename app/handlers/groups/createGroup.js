var Group = require('../../models/group');
var config = require('../../../config.js');
var secretKey = config.secretKey;

module.exports = function(req, res) {
        var group = new Group(
        {
            owner: req.decoded.id,
            groupName: req.body.groupname,
            description: req.body.description,
            autoApprove: req.body.autoapprove,
            members: req.decoded.id

        }
        );
        group.save(function(err) {
            if (err) {
                res.send(err);
                return
            }
            res.json({
                message: "Group posted!"
            });
        });
    };
    