var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var tId = req.body.groupid;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var groupId = new ObjectId(tId);
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var groups = db.collection('groups');
            groups.find({
                "_id": groupId
            }).toArray(function(err, groupExists) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (groupExists.length) {
                    //First let us check if user is blocked
                    groups.find({
                        $and: [{
                            "_id": groupId
                        }, {
                            "bannedUsers": userId
                        }]
                    }).toArray(function(err, isBlocked) {
                        if (err) {
                            res.send(err);
                        } else if (isBlocked.length) {
                            res.send("Error! You are banned from this group")

                        } else {
                            groups.find({
                                $and: [{
                                    "_id": groupId
                                }, {
                                    "members": userId
                                }]
                            }).toArray(function(err, isMember) {
                                if (err) {
                                    res.send(err);
                                } else if (isMember.length) {
                                    res.send("Error! You are already a member of this group")


                                }
                                //confirmed that no blocks exist
                                else {
                                    
                                    //let us check if there is an incoming request
                                    groups.find({
                                        $and: [{
                                                "_id": groupId
                                            }, {
                                                $or: [{
                                                    "invitedUsers": userId
                                                }, {
                                                    autoApprove: true
                                                }]
                                            }

                                        ]
                                    }).toArray(function(err, haveRevdInvitation) {

                                        if (err) {
                                            res.send(err);
                                        } else if (haveRevdInvitation.length) {
                                            //Confirming the friend request
                                            groups.update({
                                                "_id": groupId
                                            }, {
                                                $push: {
                                                    "members": userId,
                                                },
                                                $pull: {
                                                    "invitedUsers": userId
                                                }
                                            })

                                            res.send('Your membership has been confirmed');
                                        } else {
                                            //let us check if we already have a pending friend request
                                            groups.find({
                                                $and: [{
                                                    "_id": groupId
                                                }, {
                                                    "pendingUsers": userId
                                                }]
                                            }).toArray(function(err, havePendingapp) {
                                                if (err) {
                                                    res.send(err);
                                                } else if (havePendingapp.length) {
                                                    res.send('You already have already requested for membership of this group ');
                                                }
                                                //confirmed that no blocks exist
                                                else {
                                                    groups.find({
                                                        $and: [{
                                                            "_id": groupId
                                                        }, {
                                                            "members": userId
                                                        }]
                                                    }).toArray(function(err, alraedyMember) {
                                                        if (err) {
                                                            res.send(err);
                                                        } else if (alraedyMember.length) {
                                                            res.send('You are already a member of this group ');
                                                        }
                                                        //confirmed that no blocks exist
                                                        else {
                                                            //else begins
                                                            groups.update({
                                                                "_id": groupId
                                                            }, {
                                                                $push: {
                                                                    "pendingUsers": userId,
                                                                }
                                                            })



                                                            res.send('Successfully applied to this group ');
                                                        }
                                                    });
                                                    //end of else
                                                }
                                            });
                                            //end of else
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.send('The group you are attempting to join does not exist!');
                }
            });
        }
    });
};