var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var targetId = new ObjectId(tId);
    var groupId = new ObjectId(gId);

    //    var targetId = req.body.id;
    console.log(targetId);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var groups = db.collection('groups');
            groups.find({
                "_id": groupId
            }).toArray(function(err, result) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (result.length) {

                    groups.find({
                        $and: [{
                            "_id": groupId
                        }, {
                            $or: [{
                                bannedUsers: targetId
                            }]
                        }]

                    }).toArray(function(err, targetExists) {
                        if (err) {
                            res.send(err);
                        } else if (targetExists.length) {
                            res.send("Error! That user is already banned!");

                        } else {
                            groups.find({
                                $and: [{
                                        "_id": groupId
                                    }, {
                                        $or: [{
                                            "moderators": userId
                                        }, {
                                            "owner": userId
                                        }]
                                    }

                                ]
                            }).toArray(function(err, isEmpoweredtoKick) {
                                if (err) {
                                    res.send(err);
                                } else if (isEmpoweredtoKick.length) {
                                    groups.find({
                                        $and: [{
                                            "_id": groupId
                                        }, {
                                            $or: [{
                                                moderators: targetId
                                            }]
                                        }]
                                    }).toArray(function(err, isTargetEmpowered) {
                                        if (err) {
                                            res.send(err);
                                        } else if (isTargetEmpowered.length) {
                                            res.send("Error! One cannot a ban a moderator!")

                                            //updating outgoing array to resolve any bugs
                                        } else {
                                            groups.update({
                                                "_id": groupId
                                            }, {
                                                $pull: {
                                                    "members": targetId,
                                                    "moderators": targetId,
                                                    "pendingUsers": targetId,
                                                    "invitedUsers": targetId


                                                },

                                                $push: {
                                                    "bannedUsers": targetId,
                                                }


                                            });
                                            res.send("User is banned from the group!");


                                        }
                                    });
                                    //updating outgoing array to resolve any bugs
                                } else {

                                    res.send("No authority!");

                                }
                            });
                            

                        }
                    });
                } else {
                    res.send('Error! The group you are attempting to moderate does not exist!');
                }
            });
        }
    });
};