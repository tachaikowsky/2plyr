var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var targetId = new ObjectId(tId);
    var groupId = new ObjectId(gId);

    //    var targetId = req.body.id;
    console.log(targetId);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var groups = db.collection('groups');
            groups.find({
                "_id": groupId
            }).toArray(function(err, result) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (result.length) {

                    groups.find({
                        $and: [{
                            "_id": groupId
                        }, {
                            $or: [{
                                moderators: targetId
                            }]
                        }]

                    }).toArray(function(err, targetIsEmpowered) {
                        if (err) {
                            res.send(err);
                        } else if (targetIsEmpowered.length) {
                            groups.find({
                                $and: [{
                                    "_id": groupId
                                }, {
                                    $or: [{
                                        owner: userId
                                    }]
                                }]
                            }).toArray(function(err, isEmpoweredtoDisempower) {
                                if (err) {
                                    res.send(err);
                                } else if (isEmpoweredtoDisempower.length) {
                                    groups.update({
                                        "_id": groupId
                                    }, {
                                        $pull: {
                                            "moderators": targetId,
                                        }
                                    })
                                    res.send("User is demoted to member!")
                                        //updating outgoing array to resolve any bugs
                                } else {
                                    res.send("Error! You are not authorized to remove a moderator!")

                                }
                            });

                        } else {
                            res.send("Error! You are attempting to disempower the unempowered!")

                        }
                    });
                } else {
                    res.send('Error! The group you are attempting to moderate does not exist!');
                }
            });
        }
    });
};