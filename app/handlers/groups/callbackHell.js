var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

//This one isn't async coz 'map' function isn't
function jsonToNeat(groupExists, callback) {
    if (groupExists === 0)
    {
        nf = 'NF'
        callback(null, nf)
    }
    else
    {
    groupExists.map(function(obj) {
        var rObj = {};
        rObj = obj.members;
        callback(null, rObj[0]);
    });
    }
}

//this one is sync
function getGroupInfo(groupId, url, callback) {

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        } else {
            console.log('Connection established to', url);

            var groups = db.collection('groups');
            groups.find({
                    "_id": groupId
                }, {
                    "members": 1
                }

            ).limit(1).toArray(function(err, groupDetails) {

                if (err) {
                    return callback(err);
                } else if (groupDetails.length) {

                    callback(null, groupDetails);
                } else {
                    callback(null, groupDetails.length);
                }
            });

        }
    });
}



module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var userId = new ObjectId(uId);
    var groupId = new ObjectId(gId);
    getGroupInfo(groupId, url, function(err, groupDetails) {
        if (err) {
            res.send(err);
        }

        jsonToNeat(groupDetails, function(err, neatness) {

            if (err) {
                res.send(err);
            }
            res.send(neatness);
        });

});
};