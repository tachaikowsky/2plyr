var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var groupId = new ObjectId(gId);
    var invitee = new ObjectId(tId);


    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var groups = db.collection('groups');
            groups.find({
                $and: [{
                        "_id": groupId
                    }, {
                        $or: [{
                            "moderators": userId
                        }, {
                            "owner": userId
                        }]
                    }

                ]
            }).toArray(function(err, authorityExists) {

                if (err) {
                    res.send(err);
                } else if (authorityExists.length) {
                    //First let us check if user is blocked
                    groups.find({
                        $and: [{
                            "_id": groupId
                        }, {
                            "bannedUsers": invitee
                        }]
                    }).toArray(function(err, isBlocked) {
                        if (err) {
                            res.send(err);
                        } else if (isBlocked.length) {
                            res.send("Error! Member is banned from this group. Unban before inviting.")

                        } else {
                            groups.find({
                                $and: [{
                                    "_id": groupId
                                }, {
                                    "members": invitee
                                }]
                            }).toArray(function(err, isMember) {
                                if (err) {
                                    res.send(err);
                                } else if (isMember.length) {
                                    res.send("Error! The target user is already a member of this group")


                                }
                                //confirmed that no blocks exist
                                else {
                                    //let us check if there is an incoming request
                                    groups.find({
                                        $and: [{
                                                "_id": groupId
                                            }, {
                                                $or: [{
                                                    "pendingUsers": invitee
                                                }]
                                            }

                                        ]
                                    }).toArray(function(err, haveRevdApplication) {
                                        if (err) {
                                            res.send(err);
                                        } else if (haveRevdApplication.length) {
                                            //Confirming the friend request
                                            groups.update({
                                                "_id": groupId
                                            }, {
                                                $push: {
                                                    "members": invitee,
                                                },
                                                $pull: {
                                                    "pendingUsers": invitee
                                                }
                                            })

                                            res.send('The membership has been confirmed');
                                        } else {
                                            //let us check if we already have a pending friend request
                                            groups.find({
                                                $and: [{
                                                    "_id": groupId
                                                }, {
                                                    "invitedUsers": invitee
                                                }]
                                            }).toArray(function(err, invitedUsers) {
                                                if (err) {
                                                    res.send(err);
                                                } else if (invitedUsers.length) {
                                                    res.send('You already have already invited this member to this group ');
                                                }
                                                //confirmed that no blocks exist
                                                else {
                                                    groups.find({
                                                        $and: [{
                                                            "_id": groupId
                                                        }, {
                                                            "members": invitee
                                                        }]
                                                    }).toArray(function(err, alraedyMember) {
                                                        if (err) {
                                                            res.send(err);
                                                        } else if (alraedyMember.length) {
                                                            res.send('Target user is already a member of this group ');
                                                        }
                                                        //confirmed that no blocks exist
                                                        else {
                                                            //else begins
                                                            groups.update({
                                                                "_id": groupId
                                                            }, {
                                                                $push: {
                                                                    "invitedUsers": invitee,
                                                                }
                                                            })



                                                            res.send('Successfully invited to this group ');
                                                        }
                                                    });
                                                    //end of else
                                                }
                                            });
                                            //end of else
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.send('You are not authorized to invite a member');
                }
            });
        }
    });
};