var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    //var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var groupId = new ObjectId(gId);
    //var targetId = new ObjectId(tId);


    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var groups = db.collection('groups');
            groups.find({
                $and: [{
                        "_id": groupId
                    }, {
                        $or: [{
                            "owner": userId
                        }]
                    }

                ]
            }).toArray(function(err, authorityExists) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (authorityExists.length) {
                    res.send("Error! With great power comes great responsibility! Owners cannot leave group")
                } else {

                    //First let us check if user has authority
                    groups.find({
                        $and: [{
                            "_id": groupId
                        }, {
                            "members": userId
                        }]
                    }).toArray(function(err, isMember) {
                        if (err) {
                            res.send(err);
                        } else if (isMember.length) {
                            groups.update({
                                "_id": groupId
                            }, {

                                $pull: {
                                    "moderator": userId,
                                },

                                $pull: {
                                    "members": userId,
                                }




                            });
                            res.send("You have left this group!");

                        } else {
                            res.send("Error! You are not a member of this group.");

                        }
                    });
                }
            });
        }
    });
};