var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function getGroupInfo(groupId, userId, callback) {

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        } else {
            console.log('Connection established to', url);

            var groups = db.collection('groups');
            groups.find({
                    "_id": groupId
                }, {
                    "members": 1
                }

            ).limit(1).toArray(function(err, groupExists) {
                if (err) {
                    return callback(err);
                } else if (groupExists.length) {
                    gP = 'Bwahaha';
                    //In other words, do all your DB operations here and store then in variables here gP could have been a thing you got after processing data of DB too.

                    callback(null, groupExists, gP);
                    console.log('Counterterrorists win!')

                } else {
                    groupExists = 'There is no such group';
                    gP = 'LOL'
                    callback(null, groupExists, gP);
                    
                }
            });
        }
    });
}



module.exports = function(req, res) {
    var uId = req.decoded.id;
    var gId = req.body.groupid;
    
    var userId = new ObjectId(uId);
    var groupId = new ObjectId(gId);

    getGroupInfo(groupId, userId, function (err, groupDetails, gP) {
        if (err) {
            res.send(err);
        }
        console.log(gP);
        res.send(groupDetails);
    });
};
