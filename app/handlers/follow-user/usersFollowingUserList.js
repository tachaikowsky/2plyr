var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res) {
    var uId = req.body.id;
   var userId = new ObjectId(uId);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);

            var users = db.collection('users');
            users.find({
                    "followingUsers": userId
                }, {
                    "_id": 1
                }

            ).toArray(function(err, followingUsers) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (followingUsers.length) {
                    res.json(followingUsers);
                } else {
                    
                    res.send('The target user has never ever followed any user!');
                }
            });
        }
    });
};