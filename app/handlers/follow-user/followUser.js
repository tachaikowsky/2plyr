var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
module.exports = function(req, res) {
    var uId = req.decoded.id;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var targetId = new ObjectId(tId);

    //    var targetId = req.body.id;
    console.log(targetId);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var users = db.collection('users');
            users.find({
                "_id": targetId
            }).toArray(function(err, result) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (result.length) {
                    //First let us check if user is a friend
                    users.find({
                        $and: [{
                            "_id": targetId
                        }, {
                            "blockedUsers": userId
                        }]
                    }).toArray(function(err, isBlocked) {

                        if (err) {
                            res.send(err);
                        } else if (isBlocked.length) {
                            res.send("Error! You are attempting to follow a blocked user. Remove from blocklist first if you wish to follow!");
                            //updating outgoing array to resolve any bugs
                        } else {
                            users.find({
                                $and: [{
                                    "_id": userId
                                }, {
                                    "followingUsers": targetId
                                }]
                            }).toArray(function(err, haveWeFollowed) {
                                if (err) {
                                    res.send(err);
                                } else if (haveWeFollowed.length) {

                                    res.send("Error! You are attempting to follow a user whom you are already following");
                                }
                                //confirmed that no blocks exist
                                else {

                                    users.update({
                                        "_id": userId
                                    }, {

                                        $push: {
                                            "followingUsers": targetId
                                        }
                                    })

                                    res.send("You are now following this user!")
                                }
                            });
                        }
                    });
                } else {
                    res.send('The user you are attempting to follow does not exist!');
                }
            });
        }
    });
};