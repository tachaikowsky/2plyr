var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

Array.prototype.contains = function(v) {
    for(var i = 0; i < this.length; i++) {
        if(String(this[i]) === String(v)) return true;
    }
    return false;
};

Array.prototype.unique = function() {
    var arr = [];
    for(var i = 0; i < this.length; i++) {
        if(!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr; 
}

function getSentMessages(userId, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var messages = db.collection('messages');
            {
                messages.find(
                    {
                        "sender": userId
                        
                    },
                    {
                        "subject": 1,
                        "receiver": 1,
                        "_id": 0,
                    }

                ).toArray(function(err, messagesArray)
                {

                    console.log(messagesArray)
                    if (err)
                    {
                        return callback(err, null);
                    }

                    else 
                    {

                     callback(null, messagesArray);
                        //console.log('test');
                    }


                });

            }

        }

    });
}

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);
    
    console.log(userId);
        getSentMessages(userId, function(err, messagesArray)
        {
            if (err)
            {
                res.send(err);
            }
            else
            {
                console.log
                //messagesArray = messagesArray.unique();
                res.send(messagesArray);
            }
        })

    
};