var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function doesTargetUserExist(targetUserId, targetUserExistsInfo, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var users = db.collection('users');
            {
                users.find(
                    {
                        "_id": targetUserId
                    },
                    {
                        //"_id": 1
                    }

                ).limit(1).toArray(function(err, targetUserExists)
                {
                    if (err)
                    {
                        callback(err);
                    }
                    else if (targetUserExists.length)
                    {

                        targetUserExistsInfo = true;

                    }

                    return callback(null, targetUserExistsInfo);

                });



            }

        }

    });

}




function blockCheckInfo(userIdA, userIdB, blockedInfo, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var users = db.collection('users');
            {

                users.find(
                    {
                        $and: [
                        {
                            "_id": userIdA
                        },
                        {
                            "blockedUsers": userIdB
                        }]
                    },
                    {
                        //"_id": 1
                    }

                ).limit(1).toArray(function(err, targetUserhasBlocked)
                {
                    if (err)
                    {
                        callback(err);
                    }
                    else if (targetUserhasBlocked.length)
                    {

                        blockedInfo = true;
                        
                    }

                    else 
                    {

                        blockedInfo = false;
                        
                    }
                    return callback(null, blockedInfo);

                });



            }

        }

    });

}

function sendMessage(senderId, receiverId, subject, message, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);

                        return callback(err, null);
        }
        else
        {
            console.log('Connection established to', url);

         

            var messageCollection = db.collection('messages');
            var date = new Date();
            messageCollection.save(
            {
                "sender": senderId,
                "receiver": receiverId,
                "subject": subject,
                "message": message,
                "senttime": date
            })

            var endResult = "Your message was sent!";
            return callback(null, endResult);

        }
    });

};


module.exports = function(req, res)
{
    
    var uId = req.decoded.id;
    var senderId = new ObjectId(uId);

    var rId = req.body.receiver;
    var receiverId = new ObjectId(rId);

    var subject = req.body.subject;
    var message = req.body.message;

    var targetUserExistsInfo = false;
    //Does Target User Exist?
    doesTargetUserExist(receiverId, targetUserExistsInfo, function(err, targetUserExistsInfo)
    {
        if (err)
        {
            console.log(err);
        }
        else if (targetUserExistsInfo)
        {
            blockedInfo = true;
            blockCheckInfo(senderId, receiverId, blockedInfo, function(err, blockedInfo)
            {
                if (err)
                {
                    console.log(err);
                }
                else if (blockedInfo)
                {
                    res.json("Error! You have the target user in your own block list!")

                }
                else
                {
                    blockedInfo = true;
                    blockCheckInfo(receiverId, senderId, blockedInfo, function(err, blockedInfo)
                    {
                        if (err)
                        {
                            console.log(err);
                        }
                        else if (blockedInfo)
                        {
                            res.json("Error! The target user has blocked you!")

                        }
                        else
                        {

                            sendMessage(senderId, receiverId, subject, message, function(err, endResult)
                            {
                                res.json(endResult)

                            })

                        }

                    })
                    
                }

            })
        }
        else
        {
            res.json("Error! The target user doesn't exist!")
        }
    })


};