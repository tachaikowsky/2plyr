var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

Array.prototype.contains = function(v) {
    for(var i = 0; i < this.length; i++) {
        if(String(this[i]) === String(v)) return true;
    }
    return false;
};

Array.prototype.unique = function() {
    var arr = [];
    for(var i = 0; i < this.length; i++) {
        if(!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr; 
}

function getConversation(userId, receiverId, subject, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var messages = db.collection('messages');
            {
                messages.find(
                    {
                        $or: [ {$and: [
                        {
                            "sender": userId
                        },
                        {
                            "receiver": receiverId
                        },
                        {
                            "subject": subject
                        }]},
                        {$and: [
                        {
                            "sender": receiverId
                        },
                        {
                            "receiver": userId
                        },
                        {
                            "subject": subject
                        }]}
                        ]
                    },
                    {
                        message: 1,
                        subject: 1,
                        sender: 1,
                        receiver: 1,
                        senttime: 1,
                        _id: 0
                    }

                ).sort( { created: -1 }).toArray(function(err, messagesArray)
                {


                    if (err)
                    {
                        return callback(err, null);
                    }

                    else 
                    {

                     callback(null, messagesArray);
                        //console.log('test');
                    }


                });

            }

        }

    });
}

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);
    
    var rId = req.body.receiver;
    var receiverId = new ObjectId(rId);
    
    var subject = req.body.subject;
    
        getConversation(userId, receiverId, subject, function(err, messagesArray)
        {
            if (err)
            {
                res.send(err);
            }
            else
            {
                res.send(messagesArray);
            }
        })

    
};