var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

module.exports = function(req, res) {
    var uId = req.query.id;
    var friendID = new ObjectId(uId);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);
            var users = db.collection('users');
            users.find({
                    "_id": friendID
                }, {
                    "username": 1,
                    "email": 1
                }
            ).toArray(function(err, friendInfo) {
                if (err) {
                    res.send(err);
                } else if (friendInfo.length) {
                    res.json(friendInfo);
                } else {
                    res.send('You have never ever followed any user!');
                }
            });
        }
    });
};
