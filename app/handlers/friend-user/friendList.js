var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

module.exports = function(req, res) {
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);

            var users = db.collection('users');
            users.find({
                    "_id": userId
                }, {
                    "confirmedFriends": 1
                }

            ).toArray(function(err, confirmedFriends) {
                //   console.log(result);
                if (err) {
                    res.send(err);
                } else if (confirmedFriends.length) {
                    res.json(confirmedFriends);
                } else {

                    res.send('You have never ever followed any user!');
                }
            });
        }
    });
};
