var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var friendId = new ObjectId(tId);
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');
            users.find(
            {
                "_id": friendId
            }).toArray(function(err, result)
            {
                //   console.log(result);
                if (err)
                {
                    res.send(err);
                }
                else if (result.length)
                {
                    //First let us check if user is blocked
                    users.find({$and: [
                    {
                        "_id": friendId
                    },
                    {
                        "blockedUsers": userId
                    }]}).toArray(function(err, isBlocked)
                    {
                        if (err)
                        {
                            res.send(err);
                        }
                        else if (isBlocked.length)
                        {
                            res.send("Error! You are attempting to add a who has blocked you")
                                //updating outgoing array to resolve any bugs
                            users.update(
                            {
                                "_id": userId
                            },
                            {
                                $pull:
                                {
                                    "sentFriendReq": friendId,
                                    "receivedFriendReq": friendId
                                }
                            })
                            res.send('Requests to and from blocked user eliminated');
                            //updating their incoming array to resolve any bugs
                            users.update(
                            {
                                "_id": friendId
                            },
                            {
                                $pull:
                                {
                                    "sentFriendReq": userId,
                                    "receivedFriendReq": userId
                                }
                            })
                            res.send('Requests for and of blocked user eliminated');
                        }
                        else
                        {
                            users.find({$and: [
                            {
                                "_id": userId
                            },
                            {
                                "blockedUsers": friendId
                            }]}).toArray(function(err, haveWeBlocked)
                            {
                                if (err)
                                {
                                    res.send(err);
                                }
                                else if (haveWeBlocked.length)
                                {
                                    res.send("Error! You are attempting to add a user whom you have blocked")
                                        //updating outgoing array to resolve any bugs
                                    users.update(
                                    {
                                        "_id": userId
                                    },
                                    {
                                        $pull:
                                        {
                                            "sentFriendReq": friendId,
                                            "receivedFriendReq": friendId
                                        }
                                    })
                                    console.log('Requests to and from blocked user eliminated');
                                    //updating their incoming array to resolve any bugs
                                    users.update(
                                    {
                                        "_id": friendId
                                    },
                                    {
                                        $pull:
                                        {
                                            "receivedFriendReq": userId,
                                            "sentFriendReq": userId
                                        }
                                    })
                                    console.log('Requests for and of blocked user eliminated');
                                }
                                //confirmed that no blocks exist
                                else
                                {
                                    //let us check if there is an incoming request
                                    users.find({$and: [
                                    {
                                        "_id": userId
                                    },
                                    {
                                        "receivedFriendReq": friendId
                                    }]}).toArray(function(err, haveRevdFrReq)
                                    {
                                        if (err)
                                        {
                                            res.send(err);
                                        }
                                        else if (haveRevdFrReq.length)
                                        {
                                            //Confirming the friend request
                                            users.update(
                                            {
                                                "_id": userId
                                            },
                                            {
                                                $push:
                                                {
                                                    "confirmedFriends": friendId,
                                                },
                                                $pull:
                                                {
                                                    "sentFriendReq": friendId,
                                                    "receivedFriendReq": friendId
                                                }
                                            })
                                            users.update(
                                            {
                                                "_id": friendId
                                            },
                                            {
                                                $push:
                                                {
                                                    "confirmedFriends": userId,
                                                },
                                                $pull:
                                                {
                                                    "sentFriendReq": userId,
                                                    "receivedFriendReq": userId
                                                }
                                            })
                                            res.send('Your friendship has been confirmed');
                                        }
                                        
                                        else
                                        {
                                            //let us check if we already have a pending friend request
                                            users.find({$and: [
                                            {
                                                "_id": userId
                                            },
                                            {
                                                "sentFriendReq": friendId
                                            }]}).toArray(function(err, havePendingSentFrReq)
                                            {
                                                if (err)
                                                {
                                                    res.send(err);
                                                }
                                                else if (havePendingSentFrReq.length)
                                                {
                                                    res.send('You already have already requested for friendship with this user ');
                                                }
                                                //confirmed that no blocks exist
                                                else
                                                {
                                                    users.find({$and: [
                                            {
                                                "_id": userId
                                            },
                                            {
                                                "confirmedFriends": friendId
                                            }]}).toArray(function(err, alraedyFriend)
                                            {
                                                if (err)
                                                {
                                                    res.send(err);
                                                }
                                                else if (alraedyFriend.length)
                                                {
                                                    res.send('You are already friends with this user ');
                                                }
                                                //confirmed that no blocks exist
                                                else
                                                {
                                                    //else begins
                                                    users.update(
                                                    {
                                                        "_id": userId
                                                    },
                                                    {
                                                        $push:
                                                        {
                                                            "sentFriendReq": friendId,
                                                        }
                                                    })

                                                    users.update(
                                                    {
                                                        "_id": friendId
                                                    },
                                                    {
                                                        $push:
                                                        {
                                                            "receivedFriendReq": userId,
                                                        }
                                                    })

                                                    res.send('Successfully sent Friend Req');
                                                    }
                                            });
                                                    //end of else
                                                }
                                            });
                                            //end of else
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
                else
                {
                    res.send('The user you are attempting to add does not exist!');
                }
            });
        }
    });
};