var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var targetId = new ObjectId(tId);

//    var targetId = req.body.id;
        console.log(targetId);

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');
            users.find(
            {
                "_id": targetId
            }).toArray(function(err, result)
            {
                //   console.log(result);
                if (err)
                {
                    res.send(err);
                }
                else if (result.length)
                {
                    //First let us check if user is a friend
                    users.find({$and: [
                    {
                        "_id": targetId
                    },
                    {
                        "confirmedFriends": userId
                    }]}).toArray(function(err, isFriend)
                    {
                        console.log(isFriend);
                        if (err)
                        {
                            res.send(err);
                        }
                        else if (isFriend.length)
                        {
                            res.send("Error! You are attempting to block a friend. Remove from friendlist first if you wish to block!")
                                //updating outgoing array to resolve any bugs
                        }
                        else
                        {
                            users.find({$and: [
                            {
                                "_id": userId
                            },
                            {
                                "blockedUsers": targetId
                            }]}).toArray(function(err, haveWeBlocked)
                            {
                                if (err)
                                {
                                    res.send(err);
                                }
                                else if (haveWeBlocked.length)
                                {
                                    //updating outgoing array to resolve any bugs
                                    users.update(
                                        {
                                            "_id": userId
                                        },
                                        {
                                            $pull:
                                            {
                                                "sentFriendReq": targetId,
                                                "receivedFriendReq": targetId
                                            }
                                        })
                                        //updating their incoming array to resolve any bugs
                                    users.update(
                                    {
                                        "_id": targetId
                                    },
                                    {
                                        $pull:
                                        {
                                            "receivedFriendReq": userId,
                                            "sentFriendReq": userId
                                        }
                                    })
                                    res.send("Error! You are attempting to block a user whom you have already blocked")
                                }
                                //confirmed that no blocks exist
                                else
                                {
                                    //updating outgoing array to resolve any bugs
                                    users.update(
                                        {
                                            "_id": userId
                                        },
                                        {
                                            $pull:
                                            {
                                                "sentFriendReq": targetId,
                                                "receivedFriendReq": targetId
                                            },
                                            $push:
                                            {
                                                "blockedUsers": targetId
                                            }
                                        })
                                        //updating their incoming array to resolve any bugs
                                    users.update(
                                    {
                                        "_id": targetId
                                    },
                                    {
                                        $pull:
                                        {
                                            "receivedFriendReq": userId,
                                            "sentFriendReq": userId
                                        }
                                    })
                                    res.send("User is blocked")
                                }
                            });
                        }
                    });
                }
                else
                {
                    res.send('The user you are attempting to block does not exist!');
                }
            });
        }
    }); 
};