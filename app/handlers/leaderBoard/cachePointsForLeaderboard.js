var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function daysInMonth(anyDateInMonth)
{
    return new Date(anyDateInMonth.getYear(),
        anyDateInMonth.getMonth() + 1,
        0).getDate();
}

function firstDayInNextMonth(yourDate)
{
    var d = new Date(yourDate);
    d.setDate(1);
    d.setMonth(d.getMonth() + 1);
    return d;
}

function setArchivingDatesForUndefined(listOfUsersRequiringPointsArchivingFromScratch, theLastDateMonthBeforePrevious, theOldestPly, callback)
{
    var dateOfConcern = new Date(theOldestPly.getFullYear(), theOldestPly.getMonth(), 0);

    MongoClient.connect(url, function(err, db)
    {
        var done = false;
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.update(
            {
                "_id":
                {
                    $in: listOfUsersRequiringPointsArchivingFromScratch
                }
            },
            {
                $set:
                {
                    archivedTillDate: dateOfConcern
                }
            },
            {
                multi: true
            })

        }

        return callback(err, done);
    });
}



function usersRequiringPointsArchivingFromScratch(theLastDateMonthBeforePrevious, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.find(
                {

                    "archivedTillDate": null

                },
                {
                    "_id": 1,
                }

            ).toArray(function(err, listOfUsersRequiringPointsArchivingFromScratch)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null);
                }
                else
                {


                    var lOfUsersRequiringPointsArchivingFromScratch = [];

                    listOfUsersRequiringPointsArchivingFromScratch.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj._id;
                        if (rObj === undefined)
                        {
                            return callback(null, lOfUsersRequiringPointsArchivingFromScratch);
                        }
                        else
                        {
                            //tempVariable = ObjectId(rObj).toString();
                            lOfUsersRequiringPointsArchivingFromScratch.push(rObj);

                        }
                    });



                    return callback(null, lOfUsersRequiringPointsArchivingFromScratch);

                }
            });
        }
    });
}

function usersRequiringPointsArchivingUpdate(theLastDateMonthBeforePrevious, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.find(
                {


                    "archivedTillDate":
                    {
                        $lt: theLastDateMonthBeforePrevious
                    }

                },
                {
                    "_id": 1,
                }

            ).toArray(function(err, listOfUsersRequiringPointsArchivingUpdate)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null);
                }
                else
                {


                    var lOfUsersRequiringPointsArchivingUpdate = [];

                    listOfUsersRequiringPointsArchivingUpdate.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj._id;
                        if (rObj === undefined)
                        {
                            return callback(null, lOfUsersRequiringPointsArchivingUpdate);
                        }
                        else
                        {
                            tempVariable = ObjectId(rObj).toString();
                            lOfUsersRequiringPointsArchivingUpdate.push(tempVariable);

                        }
                    });


                    return callback(null, lOfUsersRequiringPointsArchivingUpdate);

                }
            });
        }
    });
}


function isThereAnArchive(isThereAnArchiveFlag, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var plies = db.collection('plies');

            plies.find(
                {},
                {
                    "_id": 0,
                    "created": 1

                }

            ).sort(
            {
                created: 1
            }).limit(1).toArray(function(err, oldestPlyInAnArray)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null, null);
                }
                else
                {

                    var now = new Date();
                    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

                    var dateOfConcern = new Date(now.getFullYear(), now.getMonth(), 1);
                    var theLastDateMonthBeforePrevious = new Date(now.getFullYear(), now.getMonth() + 1, 0);

                    var oPlyInAnArray = [];

                    oldestPlyInAnArray.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj.created;
                        if (rObj === undefined)
                        {
                            return callback(null, isThereAnArchiveFlag, theLastDateMonthBeforePrevious, oPlyInAnArray[0]);
                        }
                        else
                        {

                            oPlyInAnArray.push(rObj);

                        }
                    });


                    if (oPlyInAnArray[0] < theLastDateMonthBeforePrevious)
                    {
                        isThereAnArchiveFlag = true;
                    }
                    return callback(null, isThereAnArchiveFlag, theLastDateMonthBeforePrevious, oPlyInAnArray[0]);

                }
            });
        }
    });
}



function setArchivingDatesForIncomplete(arrayOfUsers, reverseArrayOfMonths, theLastDateMonthBeforePrevious, jugaad, callback)
{

    userId = ObjectId(arrayOfUsers[arrayOfUsers.length - 1]);

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            // console.log('Connection established to', url);

            var users = db.collection('users');

            users.find(
                {
                    "_id": userId

                },
                {
                    "_id": 0,
                    "archivedTillDate": 1
                }

            ).limit(1).toArray(function(err, archivedTillDate)
            {
                if (err)
                {
                    return callback(err, null, null);
                }
                else
                {
                    var aTillDate = [];

                    archivedTillDate.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj.archivedTillDate;

                        aTillDate.push(rObj);


                    });



                    var date1 = aTillDate[0]; //Remember, months are 0 based in JS
                    var date2 = theLastDateMonthBeforePrevious;
                    var year1 = date1.getFullYear();
                    var year2 = date2.getFullYear();
                    var month1 = date1.getMonth();
                    var month2 = date2.getMonth();
                    if (month1 === 0)
                    { //Have to take into account
                        month1++;
                        month2++;
                    }
                    var numberOfMonths;


                    numberOfMonths = (year2 - year1) * 12 + (month2 - month1);



                    var dateOfConcern_1 = new Date();
                    temp = aTillDate[0];
                    dateOfConcern_1.setDate(temp.getDate() + 1);
                    var dateOfConcern_2 = theLastDateMonthBeforePrevious;

                    var dateOfConcern_temp = temp;
                    dateOfConcern_temp.setDate(dateOfConcern_temp.getDate() + 1);

                    var dateOfCompare_temp = new Date();
                    dateOfCompare_temp.setDate(dateOfConcern_2.getDate() + 1);

                    arrayOfIndices = [];


                    while (dateOfConcern_temp.getTime() < theLastDateMonthBeforePrevious.getTime())
                    {
                        arrayOfIndices.push(daysInMonth(dateOfConcern_temp));
                        dateOfConcern_temp = firstDayInNextMonth(dateOfConcern_temp);


                    }



                    i = 0;

                    var plies = db.collection('plies');




                    plies.find(
                        {
                            "creator": userId,
                            "created":
                            {
                                $gte: dateOfConcern_1,
                                $lte: dateOfConcern_2

                            },
                            $where: "this.content.length > 40"

                        },
                        {
                            "_id": 1,
                        }

                    ).toArray(function(err, PliesArray)
                    {
                        if (err)
                        {
                            return callback(err);
                        }
                        else
                        {
                            var Prray = [];

                            PliesArray.map(function(obj)
                            {
                                var rObj = {};
                                rObj = obj._id;
                                if (rObj === undefined)
                                {
                                    Parray.push(1);
                                }
                                else
                                {

                                    Prray.push(rObj);

                                }
                            });

                            var temp = {};
                            for (var i = 0; i < Prray.length; i++)
                            {
                                temp[Prray[i]] = true;
                            }
                            var PArray = [];
                            var countArray = [];
                            for (var k in temp)
                            {
                                p = new ObjectId(k);
                                q = p.getTimestamp();

                                index = q.getDate() - dateOfConcern_1.getDate();

                                indexCleaner = index;

                                while (countArray[indexCleaner - 1] == null)
                                {
                                    // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                    indexCleaner--;
                                    if (indexCleaner == 0)
                                    {
                                        break;
                                    }

                                }

                                while (countArray[indexCleaner] == null)
                                {
                                    // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                    countArray[indexCleaner] = 0;
                                    indexCleaner++;


                                    if (indexCleaner > index)
                                    {
                                        break;
                                    }
                                }

                                if (countArray[index] < 100)
                                {
                                    countArray[index]++;
                                }

                            }

                            var sum = 0;
                            var geometricProduct = 1;
                            var arrayOfGM = [];
                            //GM logic starts here
                            for (var i = 0; i < arrayOfIndices.length; i++)
                            {
                                prodArray = [];
                                thisRoundBullets = arrayOfIndices[i];
                                for (j = thisRoundBullets; j > 0; j--)
                                {
                                    if (countArray[0] == null)
                                    {
                                        countArray[0] = 0;
                                    }
                                    sum = sum + countArray[0];
                                    prodArray.push(countArray[0]);

                                    countArray.shift();
                                }
                                doubleIndex = 2 * thisRoundBullets;
                                var average = Math.ceil(sum / doubleIndex) + 1;



                                for (var l = 0; l < prodArray.length; l++)
                                {
                                    if (prodArray[l] < average)
                                    {
                                        prodArray[l] = average;
                                    }

                                    geometricProduct = geometricProduct * prodArray[i]
                                }

                                var numberOfDaysThusFar = prodArray.length;



                                var GM_temp = Math.pow(geometricProduct, 1 / numberOfDaysThusFar) + 0.000001;

                                if (GM_temp == null)
                                {
                                    GM_temp = 0;
                                }


                                arrayOfGM.push(GM_temp);


                            }

                            var GM = 0;

                            for (m = 0; m < arrayOfGM.length - jugaad; m++)
                            {
                                if (arrayOfGM[m] == undefined)
                                {
                                    arrayOfGM[m] = 0;
                                }
                                GM = GM + arrayOfGM[m];

                            }

                            //console.log(arrayOfGM);
                            //console.log(GM);

                            reverseArrayOfMonths.push(GM);

                            users.update(
                            {
                                "_id": userId
                            },
                            {
                                $set:
                                {
                                    archivedTillDate: theLastDateMonthBeforePrevious,
                                    archivedPoints: GM
                                }
                            });




                        }


                    });



                    arrayOfUsers.pop();


                    if (arrayOfUsers.length == 0)
                    {
                        return callback(null, reverseArrayOfMonths);
                    }

                    else
                    {
                        setArchivingDatesForIncomplete(arrayOfUsers, reverseArrayOfMonths, theLastDateMonthBeforePrevious, jugaad, function(err, reverseArrayOfMonths)
                        {
                            return callback(null, reverseArrayOfMonths);

                        });

                    }

                }
            });

        }

    });
}




module.exports = function(req, res)
{

    isThereAnArchiveFlag = false;

    isThereAnArchive(isThereAnArchiveFlag, function(err, isThereAnArchiveFlag, theLastDateMonthBeforePrevious, theOldestPly)
    {

        usersRequiringPointsArchivingFromScratch(theLastDateMonthBeforePrevious, function(err, listOfUsersRequiringPointsArchivingFromScratch)
        {
            arrayOfDone = [];
            console.log(listOfUsersRequiringPointsArchivingFromScratch);

            setArchivingDatesForUndefined(listOfUsersRequiringPointsArchivingFromScratch, theLastDateMonthBeforePrevious, theOldestPly, function(err, done)
            {


                usersRequiringPointsArchivingUpdate(theLastDateMonthBeforePrevious, function(err, listOfUsersRequiringPointsArchivingUpdate)
                {
                    if (listOfUsersRequiringPointsArchivingUpdate.length != 0)
                    {
                        reverseArrayOfMonths = [];

                        listOfUsersRequiringPointsArchivingUpdate.push(listOfUsersRequiringPointsArchivingUpdate[0]);

                        jugaad = listOfUsersRequiringPointsArchivingUpdate.length;

                        setArchivingDatesForIncomplete(listOfUsersRequiringPointsArchivingUpdate, reverseArrayOfMonths, theLastDateMonthBeforePrevious, jugaad, function(err, reverseArrayOfMonths)
                        {
                            res.json(reverseArrayOfMonths);

                        });

                    }

                    else
                    {
                        res.json("Archive is already up to date!");
                    }

                });

            });
        });
    });

};