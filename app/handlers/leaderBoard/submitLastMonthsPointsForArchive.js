var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function listOfFollowedPlies(groupsArray, groupPliesArray, firstDayOfPrev, lastDayOfPrev, userId, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {

            var plies = db.collection('plies');
            var users = db.collection('users');
            {
                plies.find(
                    {
                        "creator":
                        {
                            $in: groupsArray
                        },
                        "created":
                        {
                            $gte: firstDayOfPrev,
                            $lte: lastDayOfPrev,

                        },
                        $where: "this.content.length > 40"

                    },
                    {
                        "_id": 1,
                    }

                ).toArray(function(err, PliesArray)
                {


                    if (err)
                    {
                        return callback(err);
                    }
                    else
                    {
                        var Prray = [];

                        PliesArray.map(function(obj)
                        {
                            var rObj = {};
                            rObj = obj._id;
                            if (rObj === undefined)
                            {
                                return callback(null, Prray);
                            }
                            else
                            {

                                Prray.push(rObj);

                            }
                        });

                        console.log(Prray);

                        var temp = {};
                        for (var i = 0; i < Prray.length; i++)
                        {
                            temp[Prray[i]] = true;
                        }
                        var PArray = [];
                        var countArray = [];
                        for (var k in temp)
                        {
                            p = new ObjectId(k);
                            q = p.getTimestamp();
                            index = q.getDate() - firstDayOfPrev.getDate();

                            indexCleaner = index;

                            while (countArray[indexCleaner - 1] == null)
                            {
                                // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                indexCleaner--;
                                if (indexCleaner == 0)
                                {
                                    break;
                                }

                            }

                            while (countArray[indexCleaner] == null)
                            {
                                // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                countArray[indexCleaner] = 0;
                                indexCleaner++;


                                if (indexCleaner > index)
                                {
                                    break;
                                }
                            }

                            if (countArray[index] < 100)
                            {
                                countArray[index]++;
                            }

                        }

                        var sum = 0;
                        var geometricProduct = 1;

                        for (var i = 0; i < countArray.length; i++)
                        {
                            sum = sum + countArray[i];
                        }

                        doubleIndex = 2 * countArray.length

                        var average = Math.ceil(sum / doubleIndex)

                        for (var i = 0; i < countArray.length; i++)
                        {
                            if (countArray[i] < average)
                            {
                                countArray[i] = average;
                            }

                            geometricProduct = geometricProduct * countArray[i]
                        }
                        numberOfDaysThusFar = countArray.length;



                        var GM = Math.pow(geometricProduct, 1 / numberOfDaysThusFar);



                        return callback(null, GM);

                    }


                });

            }

        }

    });
}

function numberOfFollowers(userId, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');
            users.find(
                {
                    "followingUsers": userId
                },
                {
                    "_id": 1
                }

            ).toArray(function(err, followingUsers)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null);
                }
                else
                {
                    var tempVariable = followingUsers.length;

                    //if (tempVariable < 25) {

                    //  tempVariable = 25;

                    //}

                    tempVariable = 1 + Math.pow(tempVariable, 4 / 5); // Introducing diminishing returns

                    return callback(null, tempVariable);
                }
            });
        }
    });
}

function isUserAlreadyUpdated(userId, lastDayOfPrev, isUserAlreadyUpdatedFlag, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');
            users.find(
                {
                    "_id": userId
                },
                {
                    "archivedTillDate": 1,
                    "_id": 0
                }

            ).limit(1).toArray(function(err, archivedTillD)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null);
                }
                else
                {
                    console.log(archivedTillD);
                    archivedTillD.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj.archivedTillDate;
                        console.log(rObj);
                        console.log(lastDayOfPrev);

                        var diff = Math.floor((Date.parse(rObj) - Date.parse(lastDayOfPrev)) / 86400000);

                        if (diff == 0)
                        {
                            return callback(null, true);
                        }
                        else
                        {
                            return callback(null, false);

                        }
                    });

                }
            });
        }
    });
}

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);
    var followedArray = [];
    var date = new Date();
    var firstDayOfPrev = new Date(date.getFullYear(), date.getMonth() - 1, 1);
    var lastDayOfPrev = new Date(date.getFullYear(), date.getMonth(), 0);


    selfArray = [];
    selfArray.push(userId);

    follwdPliesArray = [];
    isUserAlreadyUpdatedFlag = true;

    isUserAlreadyUpdated(userId, lastDayOfPrev, isUserAlreadyUpdatedFlag, function(err, isUserAlreadyUpdatedFlag)
    {
        if (err)
        {
            res.send(err);
        }
        else if (isUserAlreadyUpdatedFlag)
        {
            res.send("You have already updated!");
        }
        else
        {
            listOfFollowedPlies(selfArray, follwdPliesArray, firstDayOfPrev, lastDayOfPrev, userId, function(err, gmPlies)
            {
                if (err)
                {
                    res.send(err);
                }
                else
                {
                    numberOfFollowers(userId, function(err, numberFollowers)
                    {
                        var pointsEarned = numberFollowers * gmPlies;
                        pointsEarned = Math.ceil(pointsEarned * 100) / 100;


                        MongoClient.connect(url, function(err, db)
                        {
                            if (err)
                            {
                                console.log('Unable to connect to the Server', err);
                            }
                            else
                            {
                                console.log('Connection established to', url);

                                var users = db.collection('users');
                                users.update(
                                {
                                    "_id": userId

                                },
                                {
                                    $set:
                                    {
                                        "archivedTillDate": lastDayOfPrev

                                    },
                                    $inc:
                                    {
                                        "archivedPoints": pointsEarned

                                    }
                                });

                            }
                        });

                        res.json("You have submitted " + pointsEarned + " points for last month");
                    });
                }
            });
        }
    });

};