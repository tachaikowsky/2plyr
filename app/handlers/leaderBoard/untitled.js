var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function setArchivingDatesForUndefined(listOfUsersRequiringPointsArchivingFromScratch, theLastDateMonthBeforePrevious, theOldestPly, callback)
{
    var dateOfConcern = new Date(theOldestPly.getFullYear(), theOldestPly.getMonth(), 0);

    MongoClient.connect(url, function(err, db)
    {
        var done = false;
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.update(
            {
                "_id":
                {
                    $in: listOfUsersRequiringPointsArchivingFromScratch
                }
            },
            {
                $set:
                {
                    archivedTillDate: dateOfConcern
                }
            },
            {
                multi: true
            })

        }

        return callback(err, done);
    });
}



function usersRequiringPointsArchivingFromScratch(theLastDateMonthBeforePrevious, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.find(
                {

                    "archivedTillDate": null

                },
                {
                    "_id": 1,
                }

            ).toArray(function(err, listOfUsersRequiringPointsArchivingFromScratch)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null);
                }
                else
                {


                    var lOfUsersRequiringPointsArchivingFromScratch = [];

                    listOfUsersRequiringPointsArchivingFromScratch.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj._id;
                        if (rObj === undefined)
                        {
                            return callback(null, lOfUsersRequiringPointsArchivingFromScratch);
                        }
                        else
                        {
                            //tempVariable = ObjectId(rObj).toString();
                            lOfUsersRequiringPointsArchivingFromScratch.push(rObj);

                        }
                    });



                    return callback(null, lOfUsersRequiringPointsArchivingFromScratch);

                }
            });
        }
    });
}

function usersRequiringPointsArchivingUpdate(theLastDateMonthBeforePrevious, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.find(
                {


                    "archivedTillDate":
                    {
                        $lt: theLastDateMonthBeforePrevious
                    }

                },
                {
                    "_id": 1,
                }

            ).toArray(function(err, listOfUsersRequiringPointsArchivingUpdate)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null);
                }
                else
                {


                    var lOfUsersRequiringPointsArchivingUpdate = [];

                    listOfUsersRequiringPointsArchivingUpdate.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj._id;
                        if (rObj === undefined)
                        {
                            return callback(null, lOfUsersRequiringPointsArchivingUpdate);
                        }
                        else
                        {
                            tempVariable = ObjectId(rObj).toString();
                            lOfUsersRequiringPointsArchivingUpdate.push(tempVariable);

                        }
                    });


                    return callback(null, lOfUsersRequiringPointsArchivingUpdate);

                }
            });
        }
    });
}


function isThereAnArchive(isThereAnArchiveFlag, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var plies = db.collection('plies');

            plies.find(
                {},
                {
                    "_id": 0,
                    "created": 1

                }

            ).sort(
            {
                created: 1
            }).limit(1).toArray(function(err, oldestPlyInAnArray)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null, null);
                }
                else
                {

                    var now = new Date();
                    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

                    var dateOfConcern = new Date(now.getFullYear(), now.getMonth(), 1);
                    var theLastDateMonthBeforePrevious = new Date(now.getFullYear(), now.getMonth() - 1, 0);

                    var oPlyInAnArray = [];

                    oldestPlyInAnArray.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj.created;
                        if (rObj === undefined)
                        {
                            return callback(null, isThereAnArchiveFlag, theLastDateMonthBeforePrevious, oPlyInAnArray[0]);
                        }
                        else
                        {

                            oPlyInAnArray.push(rObj);

                        }
                    });


                    if (oPlyInAnArray[0] < theLastDateMonthBeforePrevious)
                    {
                        isThereAnArchiveFlag = true;
                    }
                    return callback(null, isThereAnArchiveFlag, theLastDateMonthBeforePrevious, oPlyInAnArray[0]);

                }
            });
        }
    });
}



function setArchivingDatesForIncomplete(arrayOfUsers, reverseArrayOfMonths, theLastDateMonthBeforePrevious, callback)
{

    userId = ObjectId(arrayOfUsers[arrayOfUsers.length - 1 ]);

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);

            var users = db.collection('users');

            users.find(
                {
                    "_id": userId

                },
                {
                    "_id": 0,
                    "archivedTillDate": 1
                }

            ).limit(1).toArray(function(err, archivedTillDate)
            {
                //   console.log(result);
                if (err)
                {
                    return callback(err, null, null);
                }
                else
                {
                    var aTillDate = [];

                    archivedTillDate.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj.archivedTillDate;

                        aTillDate.push(rObj);


                    });



                    var date1 = aTillDate[0]; //Remember, months are 0 based in JS
                    var date2 = theLastDateMonthBeforePrevious;
                    var year1 = date1.getFullYear();
                    var year2 = date2.getFullYear();
                    var month1 = date1.getMonth();
                    var month2 = date2.getMonth();
                    if (month1 === 0)
                    { //Have to take into account
                        month1++;
                        month2++;
                    }
                    var numberOfMonths;
                    console.log("The first date is " + date1);
                    console.log("The second date is " + date2);

                    numberOfMonths = (year2 - year1) * 12 + (month2 - month1);



                    var dateOfConcern_1 = new Date();
                    dateOfConcern_1.setDate(aTillDate[0].getDate()+1);
                    var dateOfConcern_2 = theLastDateMonthBeforePrevious;

                    console.log(dateOfConcern_1);
                    i = 0;

                    var plies = db.collection('plies');

                    console.log(userId);

                    plies.find(
                        {
                            "creator": userId

                        },
                        {
                            "_id": 1,
                        }

                    ).toArray(function(err, PliesArray)
                    {

                        console.log(PliesArray);
                        if (err)
                        {
                            return callback(err);
                        }
                        else
                        {
                            var Prray = [];

                            PliesArray.map(function(obj)
                            {
                                var rObj = {};
                                rObj = obj._id;
                                if (rObj === undefined)
                                {
                                    //return callback(null, Prray);
                                }
                                else
                                {

                                    Prray.push(rObj);

                                }
                            });

                            console.log(Prray);

                            var temp = {};
                            for (var i = 0; i < Prray.length; i++)
                            {
                                temp[Prray[i]] = true;
                            }
                            var PArray = [];
                            var countArray = [];
                            for (var k in temp)
                            {
                                p = new ObjectId(k);
                                q = p.getTimestamp();
                                console.log("attention!");
                                console.log(q);
                                console.log(dateOfConcern_1);
                                index = q.getDate() - dateOfConcern_1.getDate();
                                console.log(index);

                                indexCleaner = index;

                                while (countArray[indexCleaner - 1] == null)
                                {
                                    // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                    indexCleaner--;
                                    if (indexCleaner == 0)
                                    {
                                        break;
                                    }

                                }

                                while (countArray[indexCleaner] == null)
                                {
                                    // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                    countArray[indexCleaner] = 0;
                                    indexCleaner++;


                                    if (indexCleaner > index)
                                    {
                                        break;
                                    }
                                }

                                if (countArray[index] < 100)
                                {
                                    countArray[index]++;
                                }

                            }

                            console.log("The array of interest is " + countArray);
                            var sum = 0;
                            var geometricProduct = 1;

                            for (var i = 0; i < countArray.length; i++)
                            {
                                sum = sum + countArray[i];
                            }

                            doubleIndex = 2 * countArray.length

                            var average = Math.ceil(sum / doubleIndex)

                            for (var i = 0; i < countArray.length; i++)
                            {
                                if (countArray[i] < average)
                                {
                                    countArray[i] = average;
                                }

                                geometricProduct = geometricProduct * countArray[i]
                            }
                            console.log(countArray.length);
                            numberOfDaysThusFar = countArray.length;



                            var GM = Math.pow(geometricProduct, 1 / numberOfDaysThusFar) + 0.000001;

                            console.log("GM is " + GM);

                        }


                    });

                    console.log("Say Hi to Demon!");


                    reverseArrayOfMonths.push(numberOfMonths);
                    arrayOfUsers.pop();

                    if (arrayOfUsers.length == 0)
                    {
                        return callback(null, reverseArrayOfMonths);
                    }

                    else
                    {
                        return setArchivingDatesForIncomplete(arrayOfUsers, reverseArrayOfMonths, theLastDateMonthBeforePrevious, function(err, reverseArrayOfMonths)
                        {
                            callback(null, reverseArrayOfMonths);

                        });

                    }

                }
            });

        }

    });
}




module.exports = function(req, res)
{

    isThereAnArchiveFlag = false;

    isThereAnArchive(isThereAnArchiveFlag, function(err, isThereAnArchiveFlag, theLastDateMonthBeforePrevious, theOldestPly)
    {
        console.log("theLastDateMonthBeforePrevious is " + theLastDateMonthBeforePrevious);

        usersRequiringPointsArchivingFromScratch(theLastDateMonthBeforePrevious, function(err, listOfUsersRequiringPointsArchivingFromScratch)
        {
            arrayOfDone = [];

            setArchivingDatesForUndefined(listOfUsersRequiringPointsArchivingFromScratch, theLastDateMonthBeforePrevious, theOldestPly, function(err, done)
            {
                console.log("There were some users with undefined archive" + done)

                usersRequiringPointsArchivingUpdate(theLastDateMonthBeforePrevious, function(err, listOfUsersRequiringPointsArchivingUpdate)
                {
                    reverseArrayOfMonths = [];
                    setArchivingDatesForIncomplete(listOfUsersRequiringPointsArchivingUpdate, reverseArrayOfMonths, theLastDateMonthBeforePrevious, function(err, reverseArrayOfMonths)
                    {
                        res.json(reverseArrayOfMonths);

                    });


                    //setArchivingDatesForIncomplete(listOfUsersRequiringPointsArchivingUpdate, theLastDateMonthBeforePrevious, function(err, ) {
                    //


                    //                  });
                });

            });
        });
    });

};