var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function listOfFollowedPlies(groupsArray, groupPliesArray, callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        } else {
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate());

            var dateOfConcern = new Date(now.getFullYear(), now.getMonth(), 1);
            var plies = db.collection('plies'); {
                plies.find({
                        "creator": {
                            $in: groupsArray
                        },
                        "created": {
                            $gte: dateOfConcern
                        },
                        $where: "this.content.length > 40"

                    }, {
                        "_id": 1,
                    }

                ).toArray(function(err, PliesArray) {


                    if (err) {
                        return callback(err);
                    } else {
                        var Prray = [];

                        PliesArray.map(function(obj) {
                            var rObj = {};
                            rObj = obj._id;
                            if (rObj === undefined) {
                                return callback(null, Prray);
                            } else {

                                Prray.push(rObj);

                            }
                        });

                        console.log(Prray);

                        var temp = {};
                        for (var i = 0; i < Prray.length; i++) {
                            temp[Prray[i]] = true;
                        }
                        var PArray = [];
                        var countArray = [];
                        for (var k in temp) {
                            p = new ObjectId(k);
                            q = p.getTimestamp();
                            index = q.getDate() - dateOfConcern.getDate();

                            indexCleaner = index;
                            
                            while (countArray[indexCleaner - 1 ] == null) {
                               // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                indexCleaner--;
                                if (indexCleaner == 0)
                                {
                                    break;
                                }

                            }

                             while (countArray[indexCleaner] == null) {
                               // console.log("There is nothing at the " + indexCleaner +" position of array. Hence reloading");
                                countArray[indexCleaner] = 0;
                                indexCleaner++;


                                if (indexCleaner > index)
                                {
                                    break;
                                }
                            }   

                            if (countArray[index] < 100) {
                                countArray[index]++;
                            }

                        }

                        console.log("The array of interest is " + countArray);
                        var sum = 0;
                        var geometricProduct = 1;

                        for (var i = 0; i < countArray.length; i++) {
                            sum = sum + countArray[i];
                        }

                        doubleIndex = 2 * countArray.length

                        var average = Math.ceil(sum /  doubleIndex)

                        for (var i = 0; i < countArray.length; i++) {
                            if (countArray[i] < average) {
                                countArray[i] = average;
                            }

                            geometricProduct = geometricProduct * countArray[i]
                        }
                        console.log(countArray.length);
                        numberOfDaysThusFar = countArray.length;



                        var GM = Math.pow(geometricProduct, 1 / numberOfDaysThusFar  );

                        console.log(GM);

                        return callback(null, GM);
                    }


                });

            }

        }

    });
}

function numberOfFollowers(userId, callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        } else {
            console.log('Connection established to', url);

            var users = db.collection('users');
            users.find({
                    "followingUsers": userId
                }, {
                    "_id": 1
                }

            ).toArray(function(err, followingUsers) {
                //   console.log(result);
                if (err) {
                    return callback(err, null);
                } else {
                    var tempVariable = followingUsers.length;

                    //if (tempVariable < 25) {

                      //  tempVariable = 25;

                    //}

                    tempVariable = 1 + Math.pow(tempVariable, 4 / 5); // Introducing diminishing returns

                    return callback(null, tempVariable);
                }
            });
        }
    });
}

module.exports = function(req, res) {
    var uId = req.body.id;
    var userId = new ObjectId(uId);
    var followedArray = [];
    console.log(userId);

    selfArray = [];
    selfArray.push(userId);

    follwdPliesArray = [];

    listOfFollowedPlies(selfArray, follwdPliesArray, function(err, gmPlies) {
        if (err) {
            res.send(err);
        } else {
            numberOfFollowers(userId, function(err, numberFollowers) {
                var pointsEarned = numberFollowers * gmPlies;
                pointsEarned = Math.ceil(pointsEarned * 100) / 100;
                console.log(userId);
                res.json("This user has earned " + pointsEarned + " points this month");
            });
        }
    });

};