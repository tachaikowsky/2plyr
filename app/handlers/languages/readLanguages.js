var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;


function listLanguages(userID, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var languages = db.collection('languages');

            languages.find(
                {
                    "languagers": userID
                },
                {
                    "_id": 0,
                    "language": 1
                }
            ).toArray(function(err, listOfLanguages)
            {
                if (err)
                {
                    return callback(err, null);
                }
                else
                {
                    return callback(null, listOfLanguages);

                }
            });
        };
    });
};

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);

    listLanguages(userID, function(err, listOfLanguages)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
        	arrayOfLanguages = [];
            if (listOfLanguages)
            {
                listOfLanguages.map(function(obj)
                        {
                            var rObj = {};
                            rObj = obj.language;
                            if (rObj === undefined)
                            {
                                res.send("You don't speak any languages!")
                            }
                            else
                            {
                                arrayOfLanguages.push(rObj);
                            }
                        });
            }
            if (arrayOfLanguages.length)
            {
                res.send({ languages: arrayOfLanguages });
            }
            else
            {
                err = new Error("Error! You don't have any languages set");
                res.send(err);
            }

        };
    });
};
