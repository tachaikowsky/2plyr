var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt)
    {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}


function doesLanguageExistInDatabase(language, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var languages = db.collection('languages');

            languages.find(
                {
                    "language": language
                },
                {

                    "_id": 1
                }
            ).limit(1).toArray(function(err, languageFound)
            {
                if (err)
                {
                    return callback(err, null);
                }
                else if (languageFound.length)
                {

                    return callback(null, languageFound);

                }
                else
                {

                    languages.insert(
                    {
                        "language": language
                    });
                    languages.find(
                        {
                            "language": language
                        },
                        {

                            "_id": 1
                        }
                    ).limit(1).toArray(function(err, languageID)
                    {
                        if (err)
                        {
                            return callback(err);
                        }
                        else if (languageID.length)
                        {
                            return callback(null, languageID);
                        }
                    });
                }
            });
        }
    });
};

function insertAsLanguager(languageID, userID, insertIntoLanguageDatabase, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var languages = db.collection('languages');

            languages.find(
            {
                $and: [
                    {
                        "_id": languageID
                    },
                    {
                        "languagers": userID
                    }
                ]
            }).limit(1).toArray(function(err, insertStatus)
            {
                if (err)
                {
                    return callback(err, null, null);
                }
                else if (insertStatus.length)
                {
                    insertIntoLanguageDatabase = true;
                    var message = "Error! This language existed in your profile even before!"
                }
                else
                {
                    languages.update(
                    {
                        "_id": languageID
                    },
                    {
                        $push:
                        {
                            "languagers": userID
                        }
                    })
                    insertIntoLanguageDatabase = true;
                    var message = "Success! The language has been added to your profile"
                }

                return callback(null, insertIntoLanguageDatabase, message);
            });
        };
    });
};

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var language = toTitleCase(req.body.language);

    doesLanguageExistInDatabase(language, function(err, rawLanguageID)
    {
        if (err)
        {
            res.send(err);
        };
        var insertIntoLanguageDatabase = false;
        if (rawLanguageID)
        {
            rawLanguageID.map(function(obj)
            {
                var arrLanguageID = {};
                languageID = obj._id;
            });
        }
        else
        {
            res.send("Your request couldn't be completed. Try again Later.");
        }
        insertAsLanguager(languageID, userID, insertIntoLanguageDatabase, function(err, successfulInsertIntoLanguageDatabase, message)
        {
            if (err)
            {
                res.send(err);
            };
            if (successfulInsertIntoLanguageDatabase === true)
            {
                res.send(message);
            }
            else if (successfulInsertIntoLanguageDatabase === false)
            {
                res.send("Your request couldn't be completed. Try again Later.");
            }
        });
    });
};
