var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt)
    {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function doesLanguageExistInDatabase(language, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var languages = db.collection('languages');

            languages.find(
                {
                    "language": language
                },
                {

                    "_id": 1
                }

            ).limit(1).toArray(function(err, languageFound)
            {
                if (err)
                {
                    return callback(err, null);
                }
                else if (languageFound.length)
                {
                    return callback(null, languageFound);
                }
                else
                {
                    return callback(null, null);
                }
            });
        }
    });
};

function isUserALanguagerOfLanguage(userID, languageID, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var languages = db.collection('languages');

            languages.find(
            {
                $and: [
                    {
                        "_id": languageID
                    },
                    {
                        "languagers": userID
                    }
                ]
            }).limit(1).toArray(function(err, status)
            {
                if (err)
                {
                    return callback(err, null);
                }
                if (status.length)
                {
                    userIsLanguager = true;
                }
                else
                {
                    userIsLanguager = false;
                }

                return callback(null, userIsLanguager);
            });
        }
    });
}

function deleteUserFromLanguage(userID, languageID, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var languages = db.collection('languages');

            languages.update(
            {
                "_id": languageID
            },
            {
                $pull:
                {
                    "languagers": userID
                }
            });
        }
        languages.find(
        {
            "_id": languageID
        },
        {
            "languagers": 1,
            "_id": 0
        }).limit(1).toArray(function(err, languagersFound)
        {
            if (err)
            {
                return callback(err, null);
            }
            else
            {
                if (languagersFound.length)
                {
                    languagersFound.map(function(obj)
                    {
                        var arrLanguagersFound = {};
                        var languagersFoundAndCleaned = obj.languagers;
                        var languagerFound = languagersFoundAndCleaned[0];
                        if (languagerFound !== undefined)
                        {
                            var deleteMessage = "You were successfully deleted the language you intended"
                            return callback(null, deleteMessage);

                        }
                        else if (languagerFound === undefined)
                        {
                            languages.remove(
                                {
                                    "_id": languageID
                                }, 1
                            );
                            var deleteMessage = "You were successfully deleted the language you intended. The language has no more speakers, so it was deleted from the database"
                            return callback(null, deleteMessage);
                        }
                    });
                }
                else
                {
                    return callback(null, languagersFound);
                }
            }
        });
    });
}

module.exports = function(req, res)
{

    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var language = toTitleCase(req.body.language);

    doesLanguageExistInDatabase(language, function(err, rawLanguageID)
    {
        if (err)
        {
            res.send(err);
        };
        if (rawLanguageID)
        {
            rawLanguageID.map(function(obj)
            {
                var arrLanguageID = {};
                languageID = obj._id;
            });

            isUserALanguagerOfLanguage(userID, languageID, function(err, userIsLanguager)
            {
                if (err)
                {
                    res.send(err);
                };
                if (userIsLanguager)
                {
                    deleteUserFromLanguage(userID, languageID, function(err, deleteMessage)
                    {
                        res.send(deleteMessage);
                    });
                }
                else
                {
                    res.send("Error! You are not a speaker of the language that you are attempting to delete or replace!");
                }
            });
        }
        else
        {
            res.send("Error! The language you wanted to delete doesn't even exist in the database");
        }
    });
};
