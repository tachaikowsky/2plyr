var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';



function listOfGroups(userId, groupsArray, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var groups = db.collection('groups');

            groups.find(

                {
                    "members": userId
                },
                {
                    "_id": 1,
                }


            ).toArray(function(err, lstOfGroups)
            {


                if (err)
                {
                    return callback(err);
                }
                else if (lstOfGroups.length)
                {
                    lstOfGroups.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj._id;
                        if (rObj === undefined)
                        {
                            return callback(null, groupsArray);
                        }
                        else
                        {
                           // DrObj = new ObjectId(rObj);
                            groupsArray.push(rObj);

                        }
                    });
                }
                else
                {

                    return callback(null, groupsArray);
                }

                return callback(null, groupsArray);



            });



        }
    });

};

function listOfGroupPlies(groupsArray, groupPliesArray, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var gplies = db.collection('gplies');
            {
                gplies.find(
                    {
                        "group":
                        {
                            $in: groupsArray
                        }
                    },
                    {
                        "content": 1,
                        
                    }

                ).toArray(function(err, gPliesArray)
                {
                                        

                    if (err)
                    {
                        return callback(err);
                    }
                    
                    

                    return callback(null, gPliesArray);



                });



            }

        }

    });
}

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);
    var groupsArray = [];

    listOfGroups(userId, groupsArray, function(err, result)
    {
        if (err)
        {
            res.json(err);
        }
        groupPliesArray = [];
        listOfGroupPlies(result, groupPliesArray, function(err, gPArr)
        {
           if (err)
        {
            res.json(err);
        }
        
        res.json(gPArr); 
        })
    });


};