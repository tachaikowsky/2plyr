var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';


function accessCheckInfo(userId, groupId, accessCheckI, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var groups = db.collection('groups');
            {
                groups.find(
                {
                    $and: [
                    {
                        "_id": groupId
                    },
                    {
                        "members": userId
                    }]
                }).limit(1).toArray(function(err, accessCheck)
                {
                    if (err)
                    {
                        callback(err);
                    }
                    else if (accessCheck.length)
                    {

                        accessCheckI = true;

                    }

                    return callback(null, accessCheckI);

                });



            }

        }

    });

}

function displayGPly(groupId, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var gply = db.collection('gplies');
            
            gply.find({
                    "group": groupId,
                    "mother": {$exists: false}
                }, {
                    "_id": 0,
                    "content": 1
                }

            ).toArray(function(err, listOfPlies) {
                   console.log(listOfPlies);
                if (err) {
                    res.send(err);
                } 
                
                return callback(null, listOfPlies);    
            });

            

        }
    });

};

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);

    var gId = req.body.group;
    var groupId = new ObjectId(gId);

    accessCheck = false;

                accessCheckInfo(userId, groupId, accessCheck, function(err, accessChec)
                {
                    if (err)
                    {
                        res.send(err);
                    }

                    if (accessChec === true)
                    {
                        displayGPly( groupId, function(err, endResult)
                        {
                            if (err)
                            {
                                res.send(err);
                            }

                            res.json(endResult);

                        })
                    }
                    else if (accessChec !== true)
                    {
                        res.json('You don\'t have access to view this group');

                    }
        });

};

