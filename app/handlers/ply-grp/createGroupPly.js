var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';


function getMotherCheckInfo(motherplyId, motherCheckInfo, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var gplies = db.collection('gplies');
            {
                gplies.find(
                    {
                        "_id": motherplyId
                    }


                ).limit(1).toArray(function(err, motherExists)
                {
                    if (err)
                    {
                        callback(err);
                    }
                    else if (motherExists.length)
                    {

                        motherCheckInfo = true;

                    }

                    return callback(null, motherCheckInfo);

                });



            }

        }

    });

}


function accessCheckInfo(userId, groupId, accessCheckI, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var groups = db.collection('groups');
            {
                groups.find(
                {
                    $and: [
                    {
                        "_id": groupId
                    },
                    {
                        "members": userId
                    }]
                }).limit(1).toArray(function(err, accessCheck)
                {
                    if (err)
                    {
                        callback(err);
                    }
                    else if (accessCheck.length)
                    {

                        accessCheckI = true;

                    }

                    return callback(null, accessCheckI);

                });



            }

        }

    });

}

function saveGPly(userId, motherplyId, content, groupId, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var gply = db.collection('gplies');
            var date = new Date();
            gply.save(
            {
                "creator": userId,
                "content": content,
                "mother": motherplyId,
                "created": date,
                "group": groupId
            })

            var endResult = "The posting was a grand success!";
            return callback(null, endResult);

        }
    });

};

function saveParentGPly(userId, content, groupId, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var gply = db.collection('gplies');
            var date = new Date();
            gply.save(
            {
                "creator": userId,
                "content": content,
               
                "created": date,
                "group": groupId
            })

            var endResult = "The posting was a grand success!";
            return callback(null, endResult);

        }
    });

};

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);

    var content = req.body.content;

    var gId = req.body.group;
    var groupId = new ObjectId(gId);


    var pId = req.body.mother;
    if (pId === undefined)
    {

        accessCheck = false;

        accessCheckInfo(userId, groupId, accessCheck, function(err, accessChec)
        {
            if (err)
            {
                res.send(err);
            }

            if (accessChec === true)
            {
                saveParentGPly(userId, content, groupId, function(err, endResult)
                {
                    if (err)
                    {
                        res.send(err);
                    }

                    res.json(endResult);

                })
            }
            else if (accessChec !== true)
            {
                res.json('You don\'t have access to post in this group');

            }
        });

    }
    else if (pId !== null)
    {
        var motherplyId = new ObjectId(pId);

        motherCheck = false;

        getMotherCheckInfo(motherplyId, motherCheck, function(err, motherCheck)
        {
            if (err)
            {
                console.log(err);
            }

            if (motherCheck === true)
            {
                accessCheck = false;

                accessCheckInfo(userId, groupId, accessCheck, function(err, accessChec)
                {
                    if (err)
                    {
                        res.send(err);
                    }

                    if (accessChec === true)
                    {
                        saveGPly(userId, motherplyId, content, groupId, function(err, endResult)
                        {
                            if (err)
                            {
                                res.send(err);
                            }

                            res.json(endResult);

                        })
                    }
                    else if (accessChec !== true)
                    {
                        res.json('You don\'t have access to post in this group');

                    }
                });

            }

            else
            {
                res.send('Error! This could be mainly because the ply you are trying to reply to is probably deleted!');

            }

        });
    }

};