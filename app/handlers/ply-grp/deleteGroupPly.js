var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
var Promise = require('bluebird');


function checkForRegplyExistence(gplyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.find(
                    {
                        "mother": gplyID,
                    }).limit(1).toArray(function(err, regplyExists)
                    {
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            if (regplyExists.length)
                            {

                                var regplyExistsStatus = true;

                            }
                            else
                            {
                                var gplyExistsStatus = false;


                            }
                            resolve(regplyExistsStatus);
                        }

                    });

                }

            }

        });

    });
};

function checkForgplyExistence(gplyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.find(
                    {
                        "_id": gplyID,
                    },
                    {
                        "_id": 0,
                        "creator": 1
                    }).limit(1).toArray(function(err, gplyExists)
                    {
                        //console.log(gplyID);
                        console.log(gplyExists);
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            if (gplyExists.length)
                            {

                                gplyExists.map(function(obj)
                                {
                                    var arr = {};
                                    creatorID = obj.creator;
                                });
                                resolve(creatorID);
                            }
                            else
                            {
                                var gplyExistsStatus = false;
                                resolve(gplyExistsStatus);

                            }
                        }

                    });

                }

            }

        });

    });
};


function completeDeletegply(gplyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.remove(
                    {
                        "_id": gplyID,
                    }, 1)
                }


                var message = "Success! Your gply has been deleted";
                resolve(message);

            }

        });

    });
};


function partialDeletegply(gplyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.update(
                    {
                        "_id": gplyID,
                    },
                    {
                        $set:
                        {
                            "content": "--This gply has been deleted--"
                        }
                    })
                }
                var message = ":( Someone had replied to this gply. Only the contents of your gply could be deleted";
                resolve(message);
            }

        });

    });
};


module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var pId = req.body.gplyID;
    var gplyID = new ObjectId(pId);
    // console.log(gplyID);

    checkForgplyExistence(gplyID)
        .then(function(gplyExistsStatusWithPerm)
        {
            if (gplyExistsStatusWithPerm)
            {

                a = ObjectId(gplyExistsStatusWithPerm).toString();
                b = ObjectId(userID).toString();

                if (a == b)
                {
                    checkForRegplyExistence(gplyID)
                        .then(function(regplyExistsStatus)
                        {
                            if (regplyExistsStatus)
                            {

                                partialDeletegply(gplyID)
                                    .then(function(deleteMessage)
                                    {
                                        res.json(deleteMessage);
                                    })
                                    .catch(function(err)
                                    {
                                        res.json(err);
                                    });

                            }
                            else
                            {

                                completeDeletegply(gplyID)
                                    .then(function(deleteMessage)
                                    {
                                        res.json(deleteMessage);
                                    })
                                    .catch(function(err)
                                    {
                                        res.json(err);
                                    });

                            };
                        })
                        .catch(function(err)
                        {
                            res.json(err);
                        });
                }

                else
                {

                    res.json("Error! You are not the author of this gply");

                }


            }
            else
            {
                res.json("Error! No such gply found");
            };
        })
        .catch(function(err)
        {
            res.json(err);
        });
};