var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
var Promise = require('bluebird');


function checkForRegplyExistence(gplyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.find(
                    {
                        "mother": gplyID,
                    }).limit(1).toArray(function(err, regplyExists)
                    {
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            if (regplyExists.length)
                            {

                                var regplyExistsStatus = true;

                            }
                            else
                            {
                                var gplyExistsStatus = false;


                            }
                            resolve(regplyExistsStatus);
                        }

                    });

                }

            }

        });

    });
};

function checkForgplyExistence(gplyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.find(
                    {
                        "_id": gplyID,
                    },
                    {
                        "_id": 0,
                        "creator": 1
                    }).limit(1).toArray(function(err, gplyExists)
                    {
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            if (gplyExists.length)
                            {

                                gplyExists.map(function(obj)
                                {
                                    var arr = {};
                                    creatorID = obj.creator;
                                });
                                resolve(creatorID);
                            }
                            else
                            {
                                var gplyExistsStatus = false;
                                resolve(gplyExistsStatus);

                            }
                        }

                    });

                }

            }

        });

    });
};


function successfulEditgply(gplyID, content)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var gply = db.collection('gplies');
                {
                    gply.update(
                    {
                        "_id": gplyID,
                    },
                    {
                        $set:
                        {
                            "content": content
                        }
                    })
                }
                var message = "Your gply could be sucessfully edited";
                resolve(message);
            }

        });

    });
};


module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var pId = req.body.gplyID;
    var gplyID = new ObjectId(pId);
    var content = req.body.content;

    checkForgplyExistence(gplyID)
        .then(function(gplyExistsStatusWithPerm)
        {
            if (gplyExistsStatusWithPerm)
            {

                a = ObjectId(gplyExistsStatusWithPerm).toString();
                b = ObjectId(userID).toString();

                if (a == b)
                {
                    checkForRegplyExistence(gplyID)
                        .then(function(regplyExistsStatus)
                        {
                            if (regplyExistsStatus)
                            {

                                res.json("Error! Sorry! This gply cannot be edited because someone has already replied to it");


                            }
                            else
                            {

                                successfulEditgply(gplyID, content)
                                    .then(function(successfulEditMessage)
                                    {
                                        res.json(successfulEditMessage);
                                    })
                                    .catch(function(err)
                                    {
                                        res.json(err);
                                    });

                            };
                        })
                        .catch(function(err)
                        {
                            res.json(err);
                        });
                }

                else
                {

                    res.json("Error! You are not the author of this gply");

                }


            }
            else
            {
                res.json("Error! No such gply found");
            };
        })
        .catch(function(err)
        {
            res.json(err);
        });
};