var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var tId = req.body.id;
    var userId = new ObjectId(uId);
    //console.log(userId);
    var targetId = new ObjectId(tId);

//    var targetId = req.body.id;
        console.log(targetId);

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');
            users.find(
            {
                "_id": targetId
            }).toArray(function(err, result)
            {
                //   console.log(result);
                if (err)
                {
                    res.send(err);
                }
                else if (result.length)
                {
                    //First let us check if user is a friend
                    users.find({$and: [
                    {
                        "_id": userId
                    },
                    {
                        "blockedUsers": targetId
                    }]}).toArray(function(err, isBlocked)
                    {
                        if (err)
                        {
                            res.send(err);
                        }
                        else if (isBlocked.length)
                        {
                             users.update(
                                    {
                                        "_id": userId
                                    },
                                    {
                                        $pull:
                                        {
                                            "blockedUsers": targetId,
                                        }
                                    })
                            res.send("User unblocked!")
                                //updating outgoing array to resolve any bugs
                        }
                        else
                        {
                            res.send("Error! This user isn't blocked")
                            
                        }
                    });
                }
                else
                {
                    res.send('Error! The user you are attempting to unblock does not exist!');
                }
            });
        }
    }); 
};