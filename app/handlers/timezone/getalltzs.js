var fs = require("fs");

module.exports = function(req, res) {
    var contents = fs.readFileSync(__dirname + "/timezones.json");
    var jsonContent = JSON.parse(contents);
    return res.send(jsonContent);
}
