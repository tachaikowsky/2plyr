var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';
var Promise = require('bluebird');


function checkForReplyExistence(plyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var ply = db.collection('plies');
                {
                    ply.find(
                    {
                        "mother": plyID,
                    }).limit(1).toArray(function(err, replyExists)
                    {
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            if (replyExists.length)
                            {
                                var replyExistsStatus = true;
                            }
                            else
                            {
                                var plyExistsStatus = false;
                            }
                            resolve(replyExistsStatus);
                        }

                    });

                }

            }

        });

    });
};

function checkForPlyExistence(plyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var ply = db.collection('plies');
                {
                    ply.find(
                    {
                        "_id": plyID,
                    },
                    {
                        "_id": 0,
                        "creator": 1
                    }).limit(1).toArray(function(err, plyExists)
                    {
                        if (err)
                        {
                            reject(err);
                        }
                        else
                        {
                            if (plyExists.length)
                            {

                                plyExists.map(function(obj)
                                {
                                    var arr = {};
                                    creatorID = obj.creator;
                                });
                                resolve(creatorID);
                            }
                            else
                            {
                                var plyExistsStatus = false;
                                resolve(plyExistsStatus);
                            }
                        }

                    });

                }

            }

        });

    });
};


function completeDeletePly(plyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var ply = db.collection('plies');
                {
                    ply.remove(
                    {
                        "_id": plyID,
                    }, 1)
                }
                var message = "Success! Your ply has been deleted";
                resolve(message);

            }

        });

    });
};


function partialDeletePly(plyID)
{
    return new Promise(function(resolve, reject)
    {
        MongoClient.connect(url, function(err, db)
        {
            if (err)
            {
                console.log('Unable to connect to the Server', err);
                reject(err);
            }
            else
            {
                var ply = db.collection('plies');
                {
                    ply.update(
                    {
                        "_id": plyID,
                    },
                    {
                        $set:
                        {
                            "content": "--This ply has been deleted--"
                        }
                    })
                }
                var message = ":( Someone had replied to this ply. Only the contents of your ply could be deleted";
                resolve(message);
            }

        });

    });
};


module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var pId = req.body.plyID;
    var plyID = new ObjectId(pId);

    checkForPlyExistence(plyID)
        .then(function(plyExistsStatusWithPerm)
        {
            if (plyExistsStatusWithPerm)
            {

                a = ObjectId(plyExistsStatusWithPerm).toString();
                b = ObjectId(userID).toString();

                if (a == b)
                {
                    checkForReplyExistence(plyID)
                        .then(function(replyExistsStatus)
                        {
                            if (replyExistsStatus)
                            {

                                partialDeletePly(plyID)
                                    .then(function(deleteMessage)
                                    {
                                        res.json(deleteMessage);
                                    })
                                    .catch(function(err)
                                    {
                                        res.json(err);
                                    });

                            }
                            else
                            {

                                completeDeletePly(plyID)
                                    .then(function(deleteMessage)
                                    {
                                        res.json(deleteMessage);
                                    })
                                    .catch(function(err)
                                    {
                                        res.json(err);
                                    });

                            };
                        })
                        .catch(function(err)
                        {
                            res.json(err);
                        });
                }
                else
                {

                    res.json("Error! You are not the author of this ply");

                }
            }
            else
            {
                res.json("Error! No such Ply found");
            };          
        })
        .catch(function(err)
        {
            res.json(err);
        });
};