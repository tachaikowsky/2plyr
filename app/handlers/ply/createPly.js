var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var io = require('socket.io');
var url = config.database;


function getPlyMotherInfo(plyId, plyMotherArray, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var ply = db.collection('plies');
            {
                ply.find(
                    {
                        "_id": plyId,
                    },
                    {
                        "mother": 1,
                        _id: 0
                    }
                ).toArray(function(err, motherExists)
                {
                    if (err)
                    {
                        return callback(err);
                    }
                    else if (motherExists.length)
                    {
                        motherExists.map(function(obj)
                        {
                            var rObj = {};
                            rObj = obj.mother;

                            if (rObj === undefined)
                            {
                                var arrayOfMothers = plyMotherArray;
                                callback(null, arrayOfMothers);
                            }
                            else
                            {
                                orObj = rObj;
                                plyMotherArray.push(orObj);
                                plyId = orObj;
                                db.close();
                                return getPlyMotherInfo(plyId, plyMotherArray, function(err, arrayOfMothers)
                                {
                                    if (err)
                                    {
                                        return callback(err);
                                    }
                                    callback(null, plyMotherArray);
                                });
                            }
                        });
                    }
                    else
                    {
                        var arrayOfMothers = plyMotherArray;
                        callback(null, arrayOfMothers);
                    }
                });
            }
        }
    });
}

function getMotherCreatorInfo(plyMotherArray, motherCreatorArray, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var ply = db.collection('plies');
            {
                ply.find(
                    {
                        "_id":
                        {
                            $in: plyMotherArray
                        }
                    },
                    {
                        "creator": 1,
                        "_id": 0
                    }
                ).toArray(function(err, creatorExists)
                {
                    if (err)
                    {
                        return callback(err);
                    }
                    else if (creatorExists.length)
                    {
                        creatorExists.map(function(obj)
                        {
                            var rObj = {};
                            rObj = obj.creator;
                            if (rObj === undefined)
                            {
                                return callback(null, motherCreatorArray);
                            }
                            else
                            {
                                motherCreatorArray.push(rObj);
                            }
                        });
                    }
                    else
                    {
                        callback(null, motherCreatorArray);
                    }

                    var temp = {};
                    for (var i = 0; i < motherCreatorArray.length; i++)
                    {
                        temp[motherCreatorArray[i]] = true;
                    }
                    var mCreatorArray = [];
                    for (var k in temp)
                    {
                        p = new ObjectId(k);
                        mCreatorArray.push(p);
                    }

                    return callback(null, mCreatorArray);
                });
            }
        }
    });
}


function getMotherBlockInfo(arrayOfCreators, motherBlockArray, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var users = db.collection('users');
            {
                users.find(
                    {
                        "_id":
                        {
                            $in: arrayOfCreators
                        }
                    },
                    {
                        "blockedUsers": 1,
                        "_id": 0
                    }
                ).toArray(function(err, blockedExists)
                {
                    if (err)
                    {
                        return callback(err);
                    }
                    else if (blockedExists.length)
                    {
                        blockedExists.map(function(obj)
                        {
                            var rObj = {};
                            rObj = obj.blockedUsers;
                            if (rObj === undefined)
                            {
                                //
                                //  callback(null, motherBlockArray);
                            }
                            else
                            {
                                motherBlockArray.push(rObj);
                            }
                        });
                    }
                    else
                    {
                        //callback(null, motherBlockArray);
                    }
                    mBlockArray = [];
                    for (var i = 0; i < motherBlockArray.length; i++)
                    {
                        mTempArray = motherBlockArray[i];
                        mBlockArray = mBlockArray.concat(mTempArray);
                    }
                    var temp = {};
                    for (var i = 0; i < mBlockArray.length; i++)
                    {
                        temp[mBlockArray[i]] = true;
                    }
                    var matrBlockArray = [];
                    for (var k in temp)
                    {
                        p = new ObjectId(k);
                        matrBlockArray.push(k);
                    }
                    console.log(matrBlockArray);
                    return callback(null, matrBlockArray);
                });
            }
        }
    });
}

function getMotherCheckInfo(motherplyId, motherCheckInfo, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var plies = db.collection('plies');
            {
                plies.find(
                    {
                        "_id": motherplyId
                    },
                    {
                        //"_id": 1
                    }
                ).limit(1).toArray(function(err, motherExists)
                {
                    if (err)
                    {
                        callback(err);
                    }
                    else if (motherExists.length)
                    {
                        motherCheckInfo = true;
                    }

                    return callback(null, motherCheckInfo);
                });
            }
        }
    });
}

function savePly(userId, motherplyId, content, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');
            var ply = db.collection('plies');
            var date = new Date();
            var message = {
                "creator": userId,
                "content": content,
                "mother": motherplyId,
                "created": date
            };

            ply.save(message)
            return callback(null, message);
        }
    });
};


module.exports = function(io) {
    var _me = {};

    _me.createNewPly = function(req, res) {
        var uId = req.decoded.id;
        var userId = new ObjectId(uId);
        var content = req.body.content;
        var pId = req.body.mother;
        motherplyId = {};

        // Empty content check
        if(!content) {
            io.emit('plyCreate', { error: "Can not create ply with empty text!" });
            res.send({ error: "Can not create ply with empty text!" });
        }
        // Empty pid check to determine if existing ply or not
        else if (pId === undefined)
        {
            savePly(userId, motherplyId, content, function(err, endResult)
            {
                if (err)
                {
                    res.send(err);
                }
                io.emit('plyCreate', endResult);
                res.json(endResult);
            })
        }
        // If pid is given, reply to motherPly
        else if (pId !== null)
        {
            var motherplyId = new ObjectId(pId);
            var motherCheck = false;

            getMotherCheckInfo(motherplyId, motherCheck, function(err, motherCheck)
            {
                if (err)
                {
                    console.log(err);
                }
                if (motherCheck === true)
                {
                    var plyMotherArray = [];
                    plyMotherArray.push(motherplyId);

                    getPlyMotherInfo(motherplyId, plyMotherArray, function(err, arrayOfMothers)
                    {
                        if (err)
                        {
                            res.send(err);
                        }

                        if (arrayOfMothers.length)
                        {
                            var motherCreatorArray = [];
                            getMotherCreatorInfo(arrayOfMothers, motherCreatorArray, function(err, arrayOfCreators)
                            {
                                if (err)
                                {
                                    res.send(err);
                                }

                                var motherBlockArray = [];
                                getMotherBlockInfo(arrayOfCreators, motherBlockArray, function(err, arrayOfBlocks)
                                {
                                    if (err)
                                    {
                                        res.send(err);
                                    }

                                    var check = arrayOfBlocks.indexOf(userId);
                                    var check_red = arrayOfBlocks.indexOf(uId);
                                    blockCheckInfo = true;
                                    if (check === -1)
                                    {
                                        if (check_red === -1 )
                                        {
                                            blockCheckInfo = false;
                                        }
                                    }
                                    if (blockCheckInfo === true)
                                    {
                                        io.emit('plyCreate', 'Error! You cannot reply to this ply because at least one of the user in this plychain has blocked you!');
                                        res.json('Error! You cannot reply to this ply because at least one of the user in this plychain has blocked you!');
                                    }
                                    else
                                    {
                                        savePly(userId, motherplyId, content, function(err, endResult)
                                        {
                                            if (err)
                                            {
                                                res.send(err);
                                            }
                                            io.emit('plyCreate', endResult);
                                            res.json(endResult);
                                        })
                                    }
                                });
                            });
                        }
                        else
                        {
                            res.send('This error was never supposed to show up!!');
                        }
                    });
                }
                else
                {
                    res.send('Error! This could be mainly because the ply you are trying to reply to is probably deleted!');
                }
            });
        }
    }
    return _me;
};
