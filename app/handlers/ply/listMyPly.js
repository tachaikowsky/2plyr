var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

function listOfFollowed(userId, followedArray, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var users = db.collection('users');

            users.find(
                {
                    "_id": userId
                },
                {
                    "followingUsers": 1,
                    "_id": 0
                }
            ).limit(1).toArray(function(err, lstOfGroups)
            {
                if (err)
                {
                    return callback(err);
                }
                else if (lstOfGroups.length)
                {
                    lstOfGroups.map(function(obj)
                    {
                        var rObj = {};
                        rObj = obj.followingUsers;
                        if (rObj !== undefined)
                        {
                            followedArray.push(rObj);
                        }
                    });
                    return callback(null, followedArray);
                }
                else
                {
                    return callback(null, followedArray);
                }
            });
        }
    });
};

function listOfFollowedPlies(groupsArray, groupPliesArray, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
            return callback(err);
        }
        else
        {
            var plies = db.collection('plies');
            {
                plies.find(
                    {
                        "creator":
                        {
                            $in: groupsArray
                        }
                    },
                    {
                        "content": 1,
                        "creator": 1,
                        "_id": 0,
                        "created": 1,
                    }

                ).toArray(function(err, PliesArray)
                {
                    if (err)
                    {
                        return callback(err);
                    }
                    else if (PliesArray.length)
                    {
                     callback(null, PliesArray);
                    }
                });
            }
        }
    });
}

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userId = new ObjectId(uId);
    var followedArray = [];

    listOfFollowed(userId, followedArray, function(err, folldArray)
    {
        if (err)
        {
            res.json(err);
        }

        folldArray.push(userId);
        follwdPliesArray = [];

        listOfFollowedPlies(folldArray, follwdPliesArray, function(err, gPArr)
        {
            if (err)
            {
                res.send(err);
            }
            else
            {
                res.send(gPArr);
            }
        })
    });
};
