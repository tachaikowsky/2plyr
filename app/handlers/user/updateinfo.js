var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;
var validator = require("email-validator");

function UpdateUserEmail(userID, email, callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            console.log('Unable to connect to the Server', err);
        }
        else {
            console.log('Connection established to', url);
            var users = db.collection('users');

            users.findAndModify(
                { "_id": userID },
                [[ "_id", 1 ]],
                { $set: { "email": email }},
                {
                    upsert: true,
                    new: true
                },
                function(err, status) {
                    if(!err) {
                        return callback(null, status);
                    }
                    else {
                        console.log(err);
                        return callback(err);
                    }
                })
        }
    })
}

module.exports = function(req, res) {
    var _me = {};

    _me.updateEmail = function(req, res) {
        var uId = req.decoded.id;
        var userID = new ObjectId(uId);
        var email = req.body.email.toLowerCase();
        if(validator.validate(email)) {
            UpdateUserEmail(userID, email, function(err, callback) {
                if(!err) {
                    res.send(callback.value.email);
                }
                else {
                    res.send(err);
                }
            })
        }
    }

    return _me;
}
