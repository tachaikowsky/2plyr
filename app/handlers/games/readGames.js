var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;


function listGames(userID, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            var games = db.collection('games');
            games.find(
                {
                    "gamers": userID
                },
                {
                    "_id": 0,
                    "game": 1
                }
            ).toArray(function(err, listOfGames)
            {
                if (err)
                {
                    return callback(err, null);
                }
                else
                {
                    return callback(null, listOfGames);
                }
            });
        };
    });
};

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);

    listGames(userID, function(err, listOfGames)
    {
        if (err)
        {
            res.send(err);
        }
        else
        {
        	arrayOfGames = [];
            if (listOfGames)
            {
                listOfGames.map(function(obj)
                        {
                            var rObj = {};
                            rObj = obj.game;
                            if (rObj === undefined)
                            {
                                res.send("You don't play any games!")
                            }
                            else
                            {

                                arrayOfGames.push(rObj);

                            }
                        });
            }
            if (arrayOfGames.length)
            {
                res.send({ games: arrayOfGames });
            }
            else
            {
                res.send("Error! You don't play any games");
            }
        };
    });
};
