var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/2plyr-2devel';

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt)
    {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function doesGameExistInDatabase(game, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.find(

                {
                    "game": game
                },
                {

                    "_id": 1
                }

            ).limit(1).toArray(function(err, gameFound)
            {

                if (err)
                {
                    return callback(err, null);
                }
                else if (gameFound.length)
                {

                    return callback(null, gameFound);

                }
                else
                {

                    return callback(null, null);
                }
            });
        }
    });
};

function isUserAGamerOfGame(userID, gameID, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.find(
            {
                $and: [
                    {
                        "_id": gameID
                    },
                    {
                        "gamers": userID
                    }

                ]
            }).limit(1).toArray(function(err, status)
            {
                if (err)
                {
                    return callback(err, null);
                }

                if (status.length)
                {
                    userIsGamer = true;


                }
                else
                {
                    userIsGamer = false;

                }
                return callback(null, userIsGamer);

            });


        }
    });
}

function deleteUserFromGame(userID, gameID, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.update(
            {
                "_id": gameID
            },
            {
                $pull:
                {
                    "gamers": userID
                }
            });
        }
        games.find(
        {
            "_id": gameID
        },
        {

            "gamers": 1,
            "_id": 0
        }).limit(1).toArray(function(err, gamersFound)
        {
            if (err)
            {
                return callback(err, null);
            }
            else
            {
                if (gamersFound.length)
                {

                    gamersFound.map(function(obj)
                    {
                        var arrGamersFound = {};
                        var gamersFoundAndCleaned = obj.gamers;
                        var gamerFound = gamersFoundAndCleaned[0];
                        if (gamerFound !== undefined)
                        {
                            var deleteMessage = "The replacement was successful."
                            return callback(null, deleteMessage);

                        }
                        else if (gamerFound === undefined)
                        {
                            games.remove(
                                {
                                    "_id": gameID
                                }, 1

                            );
                            var deleteMessage = "The replacement was successful. The old game has been purged from the database since it had no gamers!"
                            return callback(null, deleteMessage);
                        }

                    });

                }
                else
                {
                    return callback(null, gamersFound);
                }
            }
        });



    });
}


function doesGameToAddExistInDatabase(game, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.find(

                {
                    "game": game
                },
                {

                    "_id": 1
                }


            ).limit(1).toArray(function(err, gameFound)
            {

                if (err)
                {
                    return callback(err, null);
                }
                else if (gameFound.length)
                {

                    return callback(null, gameFound);

                }
                else
                {

                    games.insert(
                    {
                        "game": game
                    });
                    games.find(

                        {
                            "game": game
                        },
                        {

                            "_id": 1
                        }


                    ).limit(1).toArray(function(err, gameID)
                    {

                        if (err)
                        {
                            return callback(err);
                        }
                        else if (gameID.length)
                        {

                            return callback(null, gameID);

                        }
                    });
                }
            });
        }
    });
};

function insertAsGamer(gameID, userID, insertIntoGameDatabase, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.find(
            {
                $and: [
                    {
                        "_id": gameID
                    },
                    {
                        "gamers": userID
                    }

                ]
            }).limit(1).toArray(function(err, insertStatus)
            {
                if (err)
                {
                    return callback(err, null, null);
                }
                else if (insertStatus.length)
                {
                    insertIntoGameDatabase = true;
                    var message = "Error! This game existed in your profile even before!"

                }
                else
                {
                    games.update(
                    {
                        "_id": gameID
                    },
                    {
                        $push:
                        {
                            "gamers": userID
                        }
                    })
                    insertIntoGameDatabase = true;
                    var message = "Success! The game has been added to your profile"

                }

                return callback(null, insertIntoGameDatabase, message);

            });
        };
    });
};

module.exports = function(req, res)
{

    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var gameToAdd = toTitleCase(req.body.gameToAdd);
    var gameToDelete = toTitleCase(req.body.gameToDelete);

    doesGameToAddExistInDatabase(gameToAdd, function(err, rawGameID)
    {
        if (err)
        {
            res.send(err);
        };
        var insertIntoGameDatabase = false;
        if (rawGameID)
        {
            rawGameID.map(function(obj)
            {
                var arrGameID = {};
                gametoAddID = obj._id;
            });

        }
        else
        {
            res.send("Your request couldn't be completed. Try again Later.");

        }
        insertAsGamer(gametoAddID, userID, insertIntoGameDatabase, function(err, successfulInsertIntoGameDatabase, message)
        {
            if (err)
            {
                res.send(err);
            };
            if (successfulInsertIntoGameDatabase === true)
            {
                doesGameExistInDatabase(gameToDelete, function(err, rawGameID)
                {

                    if (err)
                    {
                        res.send(err);
                    };
                    if (rawGameID)
                    {

                        rawGameID.map(function(obj)
                        {
                            var arrGameID = {};
                            gameIDToDelete = obj._id;
                        });


                        isUserAGamerOfGame(userID, gameIDToDelete, function(err, userIsGamer)
                        {
                            if (err)
                            {
                                res.send(err);
                            };
                            if (userIsGamer)
                            {
                                deleteUserFromGame(userID, gameIDToDelete, function(err, deleteMessage)
                                {
                                    res.send(deleteMessage);
                                });
                            }
                            else
                            {
                                res.send("Error! You are not a player of the game that you are attempting to delete or replace!");
                            }

                        });
                    }
                    else
                    {
                        res.send("Error! The game you wanted to delete doesn't even exist in the database");
                    }

                });
            }
            else if (successfulInsertIntoGameDatabase === false)
            {
                res.send("Your request couldn't be completed. Try again Later.");
            }

        });
    });
};