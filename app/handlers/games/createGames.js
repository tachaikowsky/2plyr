var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt)
    {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function doesGameExistInDatabase(game, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');
            games.find(
                {
                    "game": game
                },
                {
                    "_id": 1
                }
            ).limit(1).toArray(function(err, gameFound)
            {
                if (err)
                {
                    return callback(err, null);
                }
                else if (gameFound.length)
                {
                    return callback(null, gameFound);
                }
                else
                {
                    games.insert(
                    {
                        "game": game
                    });
                    games.find(
                        {
                            "game": game
                        },
                        {
                            "_id": 1
                        }
                    ).limit(1).toArray(function(err, gameID)
                    {
                        if (err)
                        {
                            return callback(err);
                        }
                        else if (gameID.length)
                        {
                            return callback(null, gameID);
                        }
                    });
                }
            });
        }
    });
};

function insertAsGamer(gameID, userID, insertIntoGameDatabase, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');
            games.find(
            {
                $and: [
                    {
                        "_id": gameID
                    },
                    {
                        "gamers": userID
                    }
                ]
            }).limit(1).toArray(function(err, insertStatus)
            {
                if (err)
                {
                    return callback(err, null, null);
                }
                else if (insertStatus.length)
                {
                    insertIntoGameDatabase = true;
                    var message = "Error! This game existed in your profile even before!"
                }
                else
                {
                    games.update(
                    {
                        "_id": gameID
                    },
                    {
                        $push:
                        {
                            "gamers": userID
                        }
                    })
                    //Emission happens here
                    insertIntoGameDatabase = true;
                    var message = "Success! The game has been added to your profile"

                }
                return callback(null, insertIntoGameDatabase, message);
            });
        };
    });
};

module.exports = function(req, res)
{
    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var game = toTitleCase(req.body.game);
    doesGameExistInDatabase(game, function(err, rawGameID)
    {
        if (err)
        {
            res.send(err);
        };
        var insertIntoGameDatabase = false;
        if (rawGameID)
        {
            rawGameID.map(function(obj)
            {
                var arrGameID = {};
                gameID = obj._id;
            });
        }
        else
        {
            res.send("Your request couldn't be completed. Try again Later.");
        }
        insertAsGamer(gameID, userID, insertIntoGameDatabase, function(err, successfulInsertIntoGameDatabase, message)
        {
            if (err)
            {
                res.send(err);
            };
            if (successfulInsertIntoGameDatabase === true)
            {
                res.send(message);
            }
            else if (successfulInsertIntoGameDatabase === false)
            {
                res.send("Your request couldn't be completed. Try again Later.");
            }
        });
    });
};
