var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var config = require('../../../config.js');
var url = config.database;

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt)
    {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

function doesGameExistInDatabase(game, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.find(

                {
                    "game": game
                },
                {

                    "_id": 1
                }

            ).limit(1).toArray(function(err, gameFound)
            {

                if (err)
                {
                    return callback(err, null);
                }
                else if (gameFound.length)
                {

                    return callback(null, gameFound);

                }
                else
                {

                    return callback(null, null);
                }
            });
        }
    });
};

function isUserAGamerOfGame(userID, gameID, callback)
{

    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.find(
            {
                $and: [
                    {
                        "_id": gameID
                    },
                    {
                        "gamers": userID
                    }

                ]
            }).limit(1).toArray(function(err, status)
            {
                if (err)
                {
                    return callback(err, null);
                }

                if (status.length)
                {
                    userIsGamer = true;


                }
                else
                {
                    userIsGamer = false;

                }
                return callback(null, userIsGamer);

            });


        }
    });
}

function deleteUserFromGame(userID, gameID, callback)
{
    MongoClient.connect(url, function(err, db)
    {
        if (err)
        {
            console.log('Unable to connect to the Server', err);
        }
        else
        {
            console.log('Connection established to', url);
            var games = db.collection('games');

            games.update(
            {
                "_id": gameID
            },
            {
                $pull:
                {
                    "gamers": userID
                }
            });
        }
        games.find(
        {
            "_id": gameID
        },
        {

            "gamers": 1,
            "_id": 0
        }).limit(1).toArray(function(err, gamersFound)
        {
            if (err)
            {
                return callback(err, null);
            }
            else
            {
                if (gamersFound.length)
                {

                    gamersFound.map(function(obj)
                    {
                        var arrGamersFound = {};
                        var gamersFoundAndCleaned = obj.gamers;
                        var gamerFound = gamersFoundAndCleaned[0];
                        if (gamerFound !== undefined)
                        {
                            var deleteMessage = "You were successfully deleted from the game you intended"
                            return callback(null, deleteMessage);

                        }
                        else if (gamerFound === undefined)
                        {
                            games.remove(
                                {
                                    "_id": gameID
                                }, 1

                            );
                            var deleteMessage = "You were successfully deleted from the game you intended. Since no one else was playing that game, it was deleted from the database"
                            return callback(null, deleteMessage);
                        }

                    });

                }
                else
                {
                    return callback(null, gamersFound);
                }
            }
        });



    });
}

module.exports = function(req, res)
{

    var uId = req.decoded.id;
    var userID = new ObjectId(uId);
    var game = toTitleCase(req.body.game);

    doesGameExistInDatabase(game, function(err, rawGameID)
    {

        if (err)
        {
            res.send(err);
        };
        if (rawGameID)
        {

            rawGameID.map(function(obj)
            {
                var arrGameID = {};
                gameID = obj._id;
            });


            isUserAGamerOfGame(userID, gameID, function(err, userIsGamer)
            {
                if (err)
                {
                    res.send(err);
                };
                if (userIsGamer)
                {
                    deleteUserFromGame(userID, gameID, function(err, deleteMessage)
                    {
                        res.send(deleteMessage);
                    });
                }
                else
                {
                    res.send("Error! You are not a player of the game that you are attempting to delete or replace!");
                }

            });
        }
        else
        {
            res.send("Error! The game you wanted to delete doesn't even exist in the database");
        }

    });
};
