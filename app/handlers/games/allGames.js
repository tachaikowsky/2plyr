var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var unirest = require('unirest');
var config = require('../../../config.js');
var url = config.database;
//var apiurl = "https://igdbcom-internet-game-database-v1.p.mashape.com/";
var apiurl = "http://www.giantbomb.com/api/";
var apiKey = "2b3ee6b8317189bc10ac0c5953302db3a0d1f7e3";
var io = require('socket.io');


/*function getPlatform(platformId, callback) {
    unirest.get(apiurl + "platforms/?fields=name&filter[id][eq]=" + platformId)
        .header("X-Mashape-Key", "m3SJjDFeA4mshnV3LwhYN4VxldGKp1VA2ihjsnZTcEXR5YmKx8")
        .header("Accept", "application/json")
        .end(function (result) {
            if (result.body) {
                var platform = result.body[0];
                return callback(null, platform.name);
            }
            else {
                return callback(null, null);
            }
        });
}

function getGames(query, callback)
{
    unirest.get(apiurl + "games/?fields=name,release_dates&filter[id][eq]=" + query + "&limit=10")
        .header("X-Mashape-Key", "m3SJjDFeA4mshnV3LwhYN4VxldGKp1VA2ihjsnZTcEXR5YmKx8")
        .header("Accept", "application/json")
        .end(function (result) {
            var listOfAllGames = result.body;
            return callback(null, listOfAllGames);
        });
};*/

function getGames(query, callback)
{
    unirest.get(apiurl + "search/?api_key=" + apiKey + "&format=json&field_list=id,name,platforms&resources=game&query=" + query)
        .header("Accept", "application/json")
        .end(function (result) {
            var listOfAllGames = [];
            if(result.body.status_code == 1) {
                listOfAllGames = result.body.results;
            }
            return callback(null, listOfAllGames);
        });
};

module.exports = function(io) {
    var _me = {};
    _me.emitAllGames = function(req, res) {
        var uId = req.decoded.id;
        var userID = new ObjectId(uId);
        var query = req.param('query');
        //var arrOfAllGames = [];

        getGames(query, function(err, listOfAllGames) {
            if(!err) {
                res.send(listOfAllGames);
            }
            /*if (err)
            {
                io.emit('allGames', 'General');
            }
            else
            {
                if (listOfAllGames) {
                    listOfAllGames.map(function(obj) {
                        var gameObj = {};

                        gameObj.id = obj.id;
                        gameObj.name = obj.name;
                        if(obj.platforms) {
                            obj.platforms.forEach(function(platform) {
                                gameObj.platform = platform.name;
                                gameObj.uniqueID = Date.now();
                                console.log(gameObj.uniqueID);
                                io.emit('allGames', gameObj);
                                arrOfAllGames.push(gameObj);
                            })
                        }
                    });
                }
                console.log(arrOfAllGames);
                res.send(arrOfAllGames);
            };*/
        });
    };
    return _me;
};
