api.post('/addFriend', function(req, res) {
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log('Unable to connect to the Server', err);
            } else {
                console.log('Connection established to', url);
                var targetUser = db.collection('users');
                
                targetUser.find({
                    "_id": req.decoded.id
                }).toArray(function(err, result) {
                    if (err) {
                        res.send(err);
                    } else if (result.length) {
                        //
                        //
                        targetUser.update({
                            "_id": req.decoded.id

                        }, {
                            $push: {
                                "arrayOfOutgoingFriends": req.body.string
                            }
                        })
                        res.send('Insertion successful and user had friends before this');

                    } else {

                        targetUser.insert({
                            "_id": req.decoded.id

                        }, {
                            $push: {
                                "arrayOfOutgoingFriends": req.body.string
                            }
                        })

                        res.send('Insertion successful and user had no friends before this');
                    }

                    
                });
            }
        });
    });