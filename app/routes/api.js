var config = require('../../config.js');
var User = require('../models/user');
var UserRatings = require('../models/userRatings');
var UserProfileLanguages = require('../models/userProfileLanguages');
var UserFriends = require('../models/userFriends');
var UserLoginLocations = require('../models/userLoginLocations');
var UserTestimonials = require('../models/userTestimonials');
var Groups = require('../models/group');
var GPly = require('../models/gply');
var PlyRatings = require('../models/plyRatings');
var secretKey = config.secretKey;
var jsonwebtoken = require('jsonwebtoken');
var http = require("http");
var moment = require('moment-timezone');
var FlakeIdGen = require('flake-idgen');
var IdBuffer = new FlakeIdGen;
var IdGen = require('biguint-format');
var playerLocator = require('../../plyrLocator/plyrLocator.js');


var addFriend = require('../handlers/friend-user/addFriend.js');
var blockUser = require('../handlers/block-user/blockUser.js');
var blockList = require('../handlers/block-user/blockList.js');

var unblockUser = require('../handlers/block-user/unblockUser.js');
var unfriendUser = require('../handlers/friend-user/unfriendUser.js');
var cancelFriendReq = require('../handlers/friend-user/cancelFriendReq.js');
var rejectFriendReq = require('../handlers/friend-user/rejectFriendReq.js');
var friendList = require('../handlers/friend-user/friendList.js');
var friendListOfUser = require('../handlers/friend-user/friendListOfUser.js');
var sentFriendReqList = require('../handlers/friend-user/sentFriendReqList.js');
var receivedFriendReqList = require('../handlers/friend-user/receivedFriendReqList.js');
var getFriendInfo = require('../handlers/friend-user/friendInfo.js');

var followUser = require('../handlers/follow-user/followUser.js');
var unfollowUser = require('../handlers/follow-user/unfollowUser.js');
var followingList = require('../handlers/follow-user/followingList.js');
var usersFollowingUserList = require('../handlers/follow-user/usersFollowingUserList.js');
var usersFollowingMeList = require('../handlers/follow-user/usersFollowingMeList.js');

var createGroup = require('../handlers/groups/createGroup.js');
var addGroupModerator = require('../handlers/groups/addGroupModerator.js');
var removeGroupModerator = require('../handlers/groups/removeGroupModerator.js');
var applyToGroup = require('../handlers/groups/applyToGroup.js');
var inviteToGroup = require('../handlers/groups/inviteToGroup.js');
var rejectApplicationToGroup = require('../handlers/groups/rejectApplicationToGroup.js');
var removeGroupMember = require('../handlers/groups/removeGroupMember.js');
var banUser = require('../handlers/groups/banUser.js');
var unbanUser = require('../handlers/groups/unbanUser.js');
var leaveGroup = require('../handlers/groups/leaveGroup.js');
var allMembersOfGroup = require('../handlers/groups/allMembersOfGroup.js');


var listMyPly = require('../handlers/ply/listMyPly.js');
var deletePly = require('../handlers/ply/deletePly.js');
var editPly = require('../handlers/ply/editPly.js');

var createGroupPly = require('../handlers/ply-grp/createGroupPly.js');
var listGroupPly = require('../handlers/ply-grp/listGroupPly.js');
var listAllGroupPly = require('../handlers/ply-grp/listAllGroupPly.js');
var deleteGroupPly = require('../handlers/ply-grp/deleteGroupPly.js');
var editGroupPly = require('../handlers/ply-grp/editGroupPly.js');

var myWeeklyPoints = require('../handlers/leaderBoard/myWeeklyPoints.js');
var myCurrentMonthlyPoints = require('../handlers/leaderBoard/myCurrentMonthlyPoints.js');
var submitLastMonthsPointsForArchive = require('../handlers/leaderBoard/submitLastMonthsPointsForArchive.js');

var usersWeeklyPoints = require('../handlers/leaderBoard/usersWeeklyPoints.js');

var cachePointsForLeaderboard = require('../handlers/leaderBoard/cachePointsForLeaderboard.js');


var sendMessage = require('../handlers/privateMessaging/sendMessage.js');
var getSentMessages = require('../handlers/privateMessaging/getSentMessages.js');
var getReceivedMessages = require('../handlers/privateMessaging/getReceivedMessages.js');
var getConversation = require('../handlers/privateMessaging/getConversation.js');



// Game APIs
var addGame = require('../handlers/games/createGames.js');
var deleteGame = require('../handlers/games/deleteGames.js');
var updateGame = require('../handlers/games/updateGames.js');
var readGames = require('../handlers/games/readGames.js');

// Language API
var addLanguage = require('../handlers/languages/createLanguages.js');
var deleteLanguage = require('../handlers/languages/deleteLanguages.js');
var updateLanguage = require('../handlers/languages/updateLanguages.js');
var listLanguages = require('../handlers/languages/readLanguages.js');
var getAllLanguages = require('../handlers/languages/getalllanguages.js');

// Time Zone API
var getAllTZs = require('../handlers/timezone/getalltzs.js');
var updateTimezone = require('../handlers/timezone/updatetimezone.js');

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = config.database;

// Update user info api


//We are using token based auth rather than cookie based auth.
function createToken(user) {
    var token = jsonwebtoken.sign({
        id: user._id,
        email: user.email,
        username: user.username
    }, secretKey, {
        expiresInMinute: 1440 //token is valid for a day
    });
    return token;
}




module.exports = function(app, express, io) {
    var api = express.Router();

    // Modules using socket are required here
    var createPly = require('../handlers/ply/createPly.js')(io);
    var listGames = require('../handlers/games/allGames.js')(io);
    var updateInfo = require('../handlers/user/updateinfo.js')();


    api.post('/signup', function(req, res) {
        console.log("A new signup request has been received");
        var now = moment(new Date());

        var getIP = require('../../plyrLocator/plyr-ipresolver')().get_ip;
        var ipInfo = getIP(req);
        var loginIP = ipInfo.clientIp;

        var idBuffer = IdBuffer.next();
        var idGen = IdGen(idBuffer, 'dec');
        //
        var user = new User({
            email: req.body.email,
            username: req.body.username,
            password: req.body.password,
            registered: now,
            registrationIp: loginIP,
            uId: idGen,
            YMoB: req.body.dob
        });

        var token = createToken(user);
        user.save(function(err) {
            if (err) {
                res.send(err);
                return;
            }
            res.json({
                success: true,
                message: 'Registration successful!!',
                token: token,
                ip: loginIP
            });
        });
    });


    api.post('/login', function(req, res) {
        var userName = req.body.username;
        User.findOne({
            username: req.body.username
        }).select('email username password').exec(function(err, user) {
            if (err) throw err;
            if (!user) {
                res.send({
                    message: "User doesn't exist!"
                });
            } else if (user) {
                var validPassword = user.comparePassword(req.body.password);
                if (!validPassword) {
                    res.send({
                        message: "Invalid password!"
                    });
                } else {
                    ///token
                    var token = createToken(user);
                    var getIP = require('../../plyrLocator/plyr-ipresolver')().get_ip;
                    var ipInfo = getIP(req);
                    var loginIP = ipInfo.clientIp;
                    var now = moment(new Date());

                    var userLoginLocation = new UserLoginLocations({

                        user: userName,
                        loginip: loginIP,
                        logintime: now
                    });
                    userLoginLocation.save(function(err) {
                        if (err) {
                            res.send(err);
                            return
                        }
                        res.json({
                            success: true,
                            message: "Successfully logged in and the token was created",
                            token: token,
                            loginip: loginIP
                        });
                    });

                }
            }
        });
    });

    //middleware starts here
    api.use(function(req, res, next) {
        var token = req.body.token || req.param('token') || req.headers['x-access-token'];
        // check if token exists
        if (token) {
            jsonwebtoken.verify(token, secretKey, function(err, decoded) {
                if (err) {
                    res.status(403).send({
                        success: false,
                        message: "Failed to authenticate user"
                    });
                } else {
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            res.status(403).send({
                success: false,
                message: "No Token Provided"
            });
        }
    });
    //middleware ends here

    api.post('/addFriend', addFriend);      // req params are sent as body because of POST req
    api.post('/blockUser', blockUser);
    api.get('/blockList', blockList);
    api.get('/friendList', friendList);     // Friend list refers to the loggedin users' list of friends
    api.get('/sentFriendReqList', sentFriendReqList);
    api.get('/receivedFriendReqList', receivedFriendReqList);
    api.get('/userInfo', getFriendInfo);    // req params are sent as query because of GET request
    api.get('/friendListOfUser', friendListOfUser);     // Friend list of user refers to friends list of some other user you are browsing

    api.post('/usersFollowingUserList', usersFollowingUserList);
    api.get('/usersFollowingMeList', usersFollowingMeList);
    api.get('/followingList', followingList);

    api.post('/unblockUser', unblockUser);
    api.post('/unfriendUser', unfriendUser);
    api.post('/cancelFriendReq', cancelFriendReq);
    api.post('/rejectFriendReq', rejectFriendReq);

    api.post('/followUser', followUser);
    api.post('/unfollowUser', unfollowUser);


    api.post('/createGroup', createGroup);
    api.post('/addGroupModerator', addGroupModerator);
    api.post('/removeGroupModerator', removeGroupModerator);
    api.post('/applyToGroup', applyToGroup);
    api.post('/inviteToGroup', inviteToGroup);
    api.post('/rejectApplicationToGroup', rejectApplicationToGroup);
    api.post('/removeGroupMember', removeGroupMember);
    api.post('/banUser', banUser);
    api.post('/unbanUser', unbanUser);
    api.post('/leaveGroup', leaveGroup);
    api.post('/allMembersOfGroup', allMembersOfGroup);


    api.post('/createPly', createPly.createNewPly);
    api.post('/listMyPly', listMyPly);
    api.post('/deletePly', deletePly);
    api.post('/editPly', editPly);

    api.post('/createGroupPly', createGroupPly);
    api.post('/listGroupPly', listGroupPly);
    api.post('/listAllGroupPly', listAllGroupPly);
    api.post('/deleteGroupPly', deleteGroupPly);
    api.post('/editGroupPly', editGroupPly);


    api.post('/myWeeklyPoints', myWeeklyPoints);
    api.post('/usersWeeklyPoints', usersWeeklyPoints);
    api.post('/myCurrentMonthlyPoints', myCurrentMonthlyPoints);
    api.post('/submitLastMonthsPointsForArchive', submitLastMonthsPointsForArchive);

    api.get('/cachePointsForLeaderboard', cachePointsForLeaderboard);



    api.post('/sendMessage', sendMessage);
    api.get('/getSentMessages', getSentMessages);
    api.get('/getReceivedMessages', getReceivedMessages);
    api.post('/getConversation', getConversation);



    api.post('/addGame', addGame);
    api.post('/deleteGame', deleteGame);
    api.post('/updateGame', updateGame);
    api.get('/readGames', readGames);
    api.get('/listGames', listGames.emitAllGames);

    api.post('/addLanguage', addLanguage);
    api.post('/deleteLanguage', deleteLanguage);
    api.post('/updateLanguage', updateLanguage);
    api.get('/listLanguages', listLanguages);
    api.get('/getAllLanguages', getAllLanguages);

    api.get('/allTZs', getAllTZs);
    api.post('/updateTimezone', updateTimezone);

    api.post('/updateEmail', updateInfo.updateEmail);

    api.get('/allMyGroups', function(req, res) {
        Groups.find({
            members: req.decoded.id
        }, function(err, userGroups) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userGroups);
        });
    });

    api.get('/allGroups', function(req, res) {
        Groups.find({
        }, function(err, userGroups) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userGroups);
        });
    });

    api.post('/allTheirGroups', function(req, res) {
        Groups.find({
            members: req.body.id
        }, function(err, userGroups) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userGroups);
        });
    });

    api.post('/addLanguage', function(req, res) {
        var userProfileLanguage = new UserProfileLanguages({
            user: req.decoded.id,
            language: req.body.language,
        });
        userProfileLanguage.save(function(err) {
            if (err) {
                res.send(err);
                return
            }
            res.json({
                message: "New Language Added!"
            });
        });
    });


    api.get('/allLanguages', function(req, res) {
        UserProfileLanguages.find({
            user: req.decoded.id
        }, function(err, userProfileLanguages) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userProfileLanguages);
        });
    });

    api.post('/addUserRating', function(req, res) {
        var userRating = new UserRatings({
            rater: req.decoded.id,
            plyer: req.body.userID,
            rating: req.body.rating
        });
        userRating.save(function(err) {
            if (err) {
                res.send(err);
                return
            }
            res.json({
                message: "The plyer has been rated!"
            });
        });
    });

    api.post('/addUserTestimonial', function(req, res) {
        var userTestimonial = new UserTestimonials({
            witness: req.decoded.id,
            plyr: req.body.userID,
            content: req.body.testimonial
        });
        userTestimonial.save(function(err) {
            if (err) {
                res.send(err);
                return
            }
            res.json({
                message: "Testimonal Submitted!"
            });
        });
    });

    api.get('/allUserRatingsOfUser', function(req, res) {
        UserRatings.find({
            rater: req.decoded.id
        }, function(err, userRatings) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userRatings);
        });
    });

    api.post('/allRatingsOfUser', function(req, res) {
        UserRatings.find({
            plyer: req.body.userID
        }, function(err, userRatings) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userRatings);
        });
    });





    api.post('/allChildrenPly', function(req, res) {
        Ply.find({
            motherply: req.body.plyID
        }, function(err, plys) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(plys);
        });
    });


    api.get('/ply', function(req, res) {
        Ply.find({
            creator: req.decoded.id
        }, function(err, plys) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(plys);
        });
    });

    api.get('/allPly', function(req, res) {
        Ply.find({}, function(err, plys) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(plys);
        });
    });

    api.get('/allUserTestimonialsOfUser', function(req, res) {
        UserTestimonials.find({
            witness: req.decoded.id
        }, function(err, userTestimonials) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userTestimonials);
        });
    });

    api.post('/allTestimonialsOfUser', function(req, res) {
        UserTestimonials.find({
            plyr: req.body.userID
        }, function(err, userTestimonials) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userTestimonials);
        });
    });

    api.get('/listUsers', function(req, res) {

        User.find({}, function(err, users) {
            if (err) {
                res.send(err);
                return;
            }
            res.json({
                success: true,
                message: "The token entered is valid - the list of users is as follows",
                "Database dump of users and their emails": users
            });
        });
    });

    api.get('/allLoginLocations', function(req, res) {
        UserLoginLocations.find({
            user: req.decoded.username
        }, function(err, userLoginLocations) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(userLoginLocations);
        });
    });
    //The distance between two users is square-root[(latitude-latitude)^2 + (longitude-longitude)^2]
    api.get('/location', function(req, res) {
        var getIP = require('../../plyrLocator/plyr-ipresolver')().get_ip;
        var ipInfo = getIP(req);
        //  console.log(getIP(req));
        var loginIP = ipInfo.clientIp;
        //  console.log(loginIP);
        playerLocator.plyrLocator({
            ip: loginIP
        }, function(err, payload) {
            res.json({
                ip: payload.ip,
                latitude: payload.latitude,
                longitude: payload.longitude
            });
        });
    });

    api.post('/addPlyRating', function(req, res) {
        var plyRating = new PlyRatings({
            rater: req.decoded.id,
            ply: req.body.plyID,
            rating: req.body.rating
        });
        plyRating.save(function(err) {
            if (err) {
                res.send(err);
                return
            }
            res.json({
                message: "The ply has been rated!"
            });
        });
    });

    api.get('/allPlyRatingsOfUser', function(req, res) {
        PlyRatings.find({
            rater: req.decoded.id
        }, function(err, plyRatings) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(plyRatings);
        });
    });

    api.post('/allRatingsOfPly', function(req, res) {
        PlyRatings.find({
            ply: req.body.plyID
        }, function(err, plyRatings) {
            if (err) {
                res.send(err);
                return;
            }
            res.json(plyRatings);
        });
    });

    api.get('/me', function(req, res) {
        res.json(req.decoded);
    });
/*
api.post('/addPly', function(req, res) {
        var ply = new GPly({
            creator: req.decoded.id,
            content: req.body.ply,
            motherply: req.body.parentply,
            group: req.body.group
        });
        ply.save(function(err) {
            if (err) {
                res.send(err);
                return
            }
            res.json({
                message: "Ply posted!"
            });
        });
    });

*/

    return api
}
