var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserTestimonialSchema = new Schema({
    plyr: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    witness: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    content: String,
    created: {
        type: String,
        default: Date.now
    }
});


module.exports = mongoose.model('UserTestimonials', UserTestimonialSchema);
