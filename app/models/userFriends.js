var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userFriendsSchema = new Schema({
	user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    arrayOfConfirmedFriends: [{
    	type: String,
    	index: {
            unique: true
        }
    }],
    arrayOfIncomingFriends: [{
    	type: Schema.Types.ObjectId,
        ref: 'User'	
    }],
    arrayOfOutgoingFriends: [{
    	type: Schema.Types.ObjectId,
        ref: 'User'	
    }],
    arrayOfBlockedUsers: [{
    	type: Schema.Types.ObjectId,
        ref: 'User'	
    }]
});

module.exports = mongoose.model('UserFriends', userFriendsSchema);
