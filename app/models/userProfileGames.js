var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userProfileGamesSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    game: {
        type: String,
        required: true
    },
    platformurl: String,
    gameprofileurl: String
});


module.exports = mongoose.model('UserProfileGames', userProfileGamesSchema);
