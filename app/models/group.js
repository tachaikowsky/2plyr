var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GroupSchema = new Schema({
    groupName: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    moderators:   [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    members:   [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    pendingUsers:   [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    invitedUsers:   [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    bannedUsers:   [{
        type: Schema.Types.ObjectId,
        ref: 'User'
    }],
    description: String,
    created: {
        type: String,
        default: Date.now
    },
    autoApprove: {
        type: Boolean,
        default: false
    },
});


module.exports = mongoose.model('Group', GroupSchema);
