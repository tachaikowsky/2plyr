var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userProfileLanguagesSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    language: {
        type: String,
        required: true
    },
    //fluency field removed because of inconsistency.
//    fluency: {
//        type: Number,
//        min: 1,
//        max: 5,
//        required: true
//    },
});


module.exports = mongoose.model('UserProfileLanguages', userProfileLanguagesSchema);
