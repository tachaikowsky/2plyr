var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var GPlySchema = new Schema({
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    mother: {
        type: Schema.Types.ObjectId,
        ref: 'Ply',
    },
    content: String,
    created: {
        type: String,
        default: Date.now
    },
    group:{
        type: Schema.Types.ObjectId,
        ref: 'Group'
    },
});


module.exports = mongoose.model('GPly', GPlySchema);
