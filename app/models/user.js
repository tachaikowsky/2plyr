var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');

var UserSchema = new Schema({
    email: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    username: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    YMoB: {
        type: Date,
        required: true
    },
    registered: {
        type: String,
    },
    registrationIp: {
        type: String,
    },
    uId: {
        type: String,
    },
    confirmedFriends: [{
        type: String
    }],
    receivedFriendReq: [{
        type: String
    }],
    sentFriendReq: [{
        type: String
    }],
    blockedUsers: [{
        type: String
    }],
    // 2 bools and 1 arrays (of sub and unsub groups)

});


UserSchema.pre('save', function(next) {
    var user = this;
    if (!user.isModified('password')) return next();
    bcrypt.hash(user.password, null, null, function(err, hash) {
        if (err) return next(err);
        user.password = hash; //if there is no error we are going to hash
        next();
    });
});


UserSchema.methods.comparePassword = function(password) {
    var user = this;
    return bcrypt.compareSync(password, user.password);
}


module.exports = mongoose.model('User', UserSchema); //passing UserSchema as User
