var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userLoginLocationsSchema = new Schema({
    user: {
        type: String
    },
    loginip: {
        type: String
    },
	logintime: {
        type: String,
    }
});


module.exports = mongoose.model('UserLoginLocations', userLoginLocationsSchema);
