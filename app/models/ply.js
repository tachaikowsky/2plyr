var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PlySchema = new Schema({
    creator: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    mother: {
        type: Schema.Types.ObjectId,
        ref: 'Ply',
    },
    content: String,
    created: {
        type: String,
        default: Date.now
    },
});


module.exports = mongoose.model('Ply', PlySchema);
