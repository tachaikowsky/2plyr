var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var plyRatingSchema = new Schema({
    rater: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    ply: {
        type: Schema.Types.ObjectId,
        ref: 'Ply'
    },

    rating: {
                type: String,
    },

});


module.exports = mongoose.model('plyRatings', plyRatingSchema);
