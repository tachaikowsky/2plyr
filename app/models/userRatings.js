var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userRatingSchema = new Schema({
    rater: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    plyer: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },

    rating: {
                type: String,
    },

});


module.exports = mongoose.model('userRatings', userRatingSchema);
