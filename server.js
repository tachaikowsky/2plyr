var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var config = require('./config.js');
var mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var url = config.database;
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

//connecting to the mongoose db
mongoose.connect(config.database, function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log("Connected to the mongoose database");
    }
});

MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server without using mongoose.");

});



//express.js
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use(morgan('dev'));

app.use(express.static(__dirname + '/public'));

//using api
var api = require('./app/routes/api')(app, express, io);
app.use('/api', api);

app.get('*', function(req, res){
    res.sendFile(__dirname + '/public/app/views/index.html');
});

//the web-app is listening on specific port
http.listen(config.port, function(err) { //http will give us realtime capability
    if (err) {
        console.log(err);
    } else {
        console.log("The web application is listening on port " + config.port);
    }
});
