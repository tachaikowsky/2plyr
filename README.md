# API spec

Method | Path | Params | Returns
-------|------|--------| -------
POST | /api/signup | username, password, dob, name, email | token
POST | /api/login | username, password | token
GET | /api/me | token | Info about loggedin user
--- |
POST | /api/addGame | token, game | Adds a new game to the database / Adds user id to game's list
POST | /api/listGames | token | List of games
GET | /api/allGames | token, query (yet to do something) | Serve list of all existing games in database
POST | /api/deleteGame | token, game | Deletes a game from database
--- |
GET | /api/friendListOfUser | token | unknown
GET | /api/friendList | token | Friends list of loggedin user
POST | /api/addFriend | token, id (friend's id) | true/false
GET | /api/receivedFriendReqList | token | Incoming friend requests
--- |
POST | /api/addLanguage | token, language | Adds a user to a language / Adds new language if does not exist
POST | /api/deleteLanguage | token, language | Delete language
GET | /api/listLanguages | token | Get list of all languages a user speak

### If database is updated then to retrive updated record we should access that data with new user token
