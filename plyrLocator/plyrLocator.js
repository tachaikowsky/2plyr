var path = require('path');
var mmdbreader = require('maxmind-db-reader');


function PlyrLocator() {
    this.init();
}

PlyrLocator.prototype.init = function() {
    this.db = mmdbreader.openSync(path.join(__dirname, '/DB/geo-ip-db.mmdb'));
    this.initialized = true;
};

PlyrLocator.prototype.plyrLocator = function(options, next) {
    if (!this.initialized) {
        return next(new Error('The geo ip database is not loaded'));
    }

    var data = this.db.getGeoDataSync(options.ip);

    if (data) {
        return next(null, {
            ip: options.ip,
            continent_code: data.continent.code,
            continent: data.continent.names,
            country_code: data.country.iso_code,
            country: data.country.names,
            latitude: data.location.latitude,
            longitude: data.location.longitude,
            timezone: data.location.time_zone
        });
    }

    return next(null, null);

};

module.exports = new PlyrLocator();