angular.module('plyService', [])

.factory('Ply', function($http){

    var plyFactory = {};

    plyFactory.allPlys = function(AuthToken) {
        return $http.post('/api/listMyPly', AuthToken);
    }
    plyFactory.create = function(AuthToken, plyData) {
        return $http.post('/api/createPly', { content: plyData });
    }

    return plyFactory;
})

.factory('socketio', function($rootScope) {

    var socket = io.connect();
    return {//A different way to create a functions

        on: function(eventName, callback) {
          socket.on(eventName, function() {
              var args = arguments;
              $rootScope.$apply(function(){
                  callback.apply(socket, args);
              });
          });
        },

        emit: function(eventName, data, callback) {
          socket.emit(eventName, data, function() {
              var args = arguments;
              $rootScope.apply(function(){
                if(callback) {
                  callbac.apply(socket, args);
                }
              });
          });
        }
    };
});
