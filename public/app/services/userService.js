angular.module('userService', [])


.factory('User', function($http) {

    var userFactory = {};

    userFactory.create = function(userData) {
        return $http.post('/api/signup', userData);
    }

    userFactory.all = function() {
        return $http.get('/api/users');
    }

    // Freiend functions
    userFactory.getFriends = function(AuthToken) {
        return $http.get('/api/friendList', AuthToken);
    }
    userFactory.getFriendRequests = function(AuthToken) {
        return $http.get('/api/receivedFriendReqList', AuthToken);
    }
    userFactory.addFriend = function(AuthToken, firendID) {
        return $http.post('/api/addFriend', { params: { 'id': firendID } });
    }
    userFactory.getFriendInfo = function(AuthToken, firendID) {
        return $http.get('/api/userInfo', { params: { 'id': firendID } });
    }

    // Game functions
    userFactory.getAllGames = function(AuthToken, Query) {
        return $http.get('/api/listGames', { params: { 'query': Query } });
    }
    userFactory.addGame = function(AuthToken, Game) {
        return $http.post('/api/addGame', { game: Game });
    }
    userFactory.removeGame = function(AuthToken, Game) {
        return $http.post('/api/deleteGame', { game: Game });
    }
    userFactory.getGames = function(AuthToken) {
        return $http.get('/api/readGames', AuthToken);
    }

    // Language functions
    userFactory.getLanguages = function(AuthToken) {
        return $http.get('/api/listLanguages', AuthToken);
    }
    userFactory.addLanguage = function(AuthToken, Language) {
        return $http.post('/api/addLanguage', { language: Language });
    }
    userFactory.removeLanguage = function(AuthToken, Language) {
        return $http.post('/api/deleteLanguage', { language: Language })
    }
    userFactory.getAllLanguages = function(AuthToken) {
        return $http.get('/api/getAllLanguages', AuthToken);
    }
    userFactory.getAllTZ = function(AuthToken) {
        return $http.get('/api/allTZs', AuthToken);
    }

    userFactory.updateTZ = function(AuthToken, Timezone) {
        return $http.post('/api/updateTimezone', { timezone: Timezone });
    }

    return userFactory;
})

.factory('socketio', function($rootScope) {

    var socket = io.connect();
    return {//A different way to create a functions

        on: function(eventName, callback) {
          socket.on(eventName, function() {
              var args = arguments;
              $rootScope.$apply(function(){
                  callback.apply(socket, args);
              });
          });
        },

        emit: function(eventName, data, callback) {
          socket.emit(eventName, data, function() {
              var args = arguments;
              $rootScope.apply(function(){
                if(callback) {
                  callbac.apply(socket, args);
                }
              });
          });
        }
    };
});
