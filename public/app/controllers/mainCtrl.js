angular.module('mainCtrl', ['userService'])

.controller('MainController', function($rootScope, $location, Auth, User, AuthToken, socketio) {

    var vm = this;

    vm.loggedIn = Auth.isLoggedIn();
    vm.allGames = [];
    vm.uniqueGames = [];
    vm.allLanguages = {};

    $rootScope.$on('$routeChangeStart', function() { //Is the route changing?

        vm.loggedIn = Auth.isLoggedIn();
        vm.allGames = [];

        Auth.getUser()
            .then(function(data) {
                vm.user = data.data;
            });
        User.getFriendRequests(AuthToken)
            .success(function(data) {
                vm.user.friendRequests = data[0].receivedFriendReq.slice();
            })
    });

    vm.doLogin = function() {
        vm.processing = true;
        vm.error = '';
        Auth.login(vm.loginData.username, vm.loginData.password)
            .success(function(data) {
                vm.processing = false;

                Auth.getUser()
                    .then(function(data) {
                        vm.user = data.data;
                    });

                if (data.success)
                    $location.path('/');
                else
                    vm.error = data.message;
            });
    }

    vm.doLogout = function() {
        Auth.logout();
        $location.path('/logout');
    }

    vm.friendAddAccept = function(friendID) {
        User.addFriend(AuthToken, friendID)
            .success(function(data) {
                console.log(data);
            })
    }

    vm.loadGames = function(query) {
        User.getAllGames(AuthToken, query)
            .success(function(arrOfAllGames) {
                if (arrOfAllGames) {
                    vm.cleanArray(arrOfAllGames, function(err, arrOfGames) {
                        if(!err) {
                            if(vm.allGames.length > 100) {
                                vm.allGames.shift();
                            }
                            if(arrOfGames.platform) {
                                vm.allGames.push({ "text": arrOfGames.name + "-" + arrOfGames.platform });
                            }
                            else {
                                vm.allGames.push({ "text": arrOfGames.name });
                            }
                        }
                    })
                }
            })
            for(var i=0; i<vm.allGames.length; i++) {
                if(vm.uniqueGames.length > 100) {
                    vm.uniqueGames.shift();
                }
                vm.uniqueGames.push({ id: i, text: vm.allGames[i].text });
            }
        console.log(vm.uniqueGames);
        return vm.uniqueGames;
    }

    vm.cleanArray = function(req, callback) {
        var unique = {};
        req.map(function(obj) {
            var gameObj = {};

            gameObj.id = obj.id;
            gameObj.name = obj.name;
            if(obj.platforms) {
                obj.platforms.forEach(function(platform) {
                    if(!unique[platform.name] && !unique[gameObj.id]) {
                        unique[platform.name] = platform.name;
                        unique[gameObj.id] = gameObj.id;
                        gameObj.platform = platform.name;
                    }
                })
                callback(null, gameObj);
            }
        });
    }

    /*socketio.on('allGames', function(data) {
        if (data) {
            gameString = data.name + ' - ' + data.platform;
            vm.allGames.push({ "text": gameString, "id": data.uniqueID });
            console.log(vm.allGames);
        }
    })*/
})
