angular.module('plyCtrl', ['plyService'])

    .controller('PlyController', function(Ply, AuthToken, socketio) {

        var vm = this;

        Ply.allPlys()
            .success(function(data) {
                for (i=0; i < data.length; i++) {
                    data[i].created = data[i].created.replace(/(\.[0-9])\w+/g,"");
                    data[i].created = data[i].created.replace(/T/g," at ");
                }
                vm.plyCollection = data;
            });

        vm.createPly = function() {
            vm.message = '';
            Ply.create(AuthToken, vm.newPly)
              .success(function(data){
                vm.newPly = '';
                vm.message = data.message;
              });
        };

        socketio.on('plyCreate', function(data) {
            data.created = data.created.replace(/(\.[0-9])\w+/g,"");
            data.created = data.created.replace(/T/g," at ");
            vm.plyCollection.push(data);
        })
    })
