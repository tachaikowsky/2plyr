angular.module('profileCtrl', ['userService'])

    .controller('ProfileController', function(User, AuthToken) {
        var vm = this;

        User.getGames(AuthToken)
            .success(function(data) {
                vm.profileGames = data.games.slice();
            });
        User.getFriends(AuthToken)
            .success(function(data) {
                vm.profileFriends = data[0].confirmedFriends.slice();
                vm.profileFriendsInfo = [];
                for (var friend in vm.profileFriends) {
                    User.getFriendInfo(AuthToken, vm.profileFriends[friend])
                        .success(function(data) {
                            vm.profileFriendsInfo[data[0]._id] = data;
                        })
                }
            });
        User.getLanguages(AuthToken)
            .success(function(data) {
                vm.userLanguages = data.languages.slice();
            })

        User.getAllLanguages(AuthToken)
            .success(function(allLanguages) {
                console.log(allLanguages);
                vm.allLanguages = allLanguages;
            })

        User.getAllTZ(AuthToken)
            .success(function(allTZ) {
                vm.allTZ = allTZ;
            })

        vm.onGameAdded = function(tag) {
            User.addGame(AuthToken, tag.text)
                .success(function(data) {
                    console.log(data);
                })
        }

        vm.onGameRemoved = function(tag) {
            User.removeGame(AuthToken, tag.text)
                .success(function(data) {
                    console.log(data);
                })
        }

        vm.onLanguageAdded = function(tag) {
            User.addLanguage(AuthToken, tag.text)
                .success(function(data) {
                    console.log(data);
                })
        }

        vm.onLanguageRemoved = function(tag) {
            User.removeLanguage(AuthToken, tag.text)
                .success(function(data) {
                    console.log(data);
                })
        }

        vm.updateProfile = function() {
            User.getUserTZ(AuthToken)
                .success(function(oldUserTZ) {
                    if(vm.userTZ !== oldUserTZ) {
                        User.updateTZ(AuthToken, vm.userTZ)
                            .success(function(data) {
                                console.log("Time zone updated. Resp: " + data);
                            })
                    }
                })
            console.log("Profile updated");
        }
    });
