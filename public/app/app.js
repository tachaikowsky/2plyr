angular.module('MyApp', ['appRoutes', 'authService', 'mainCtrl', 'userService', 'userCtrl', 'profileCtrl', 'plyCtrl', 'ngTagsInput'])

.config(function($httpProvider) {

    $httpProvider.interceptors.push('AuthInterceptor');

})

.filter('reverse', function() {
    return function(items) {
        if(items) {
            return items.slice().reverse();
        }
    };
})
